/*******************************************************************************
 * @file	    : bootstrap.c
 * @brief	    : This source file contains code for creating socket which will
 *                listen for incoming discovery request from AP and does
 *                load balancing on available CVM's and sends CVM IP as
 *                response vdn page from server to the client.
 * @author	    : Balaji MH (balaji.mh@vvdntech.in)
 * @copyright   : (c) 2017-2018, VVDN Technologies Pvt. Ltd.
 *                Permission is hereby granted to everyone in VVDN Technologies
 *                to use the Software without restriction, including without
 *                limitation the rights to use, copy, modify, merge, publish,
 *                distribute, distribute with modifications.
 ******************************************************************************/

#include "wcm.h"
#include "bootstrap.h"
#include "wcm_write.h"
#include "cass_prototypes.h"
#include "wlc_cass_error_strings.h"
extern struct wlcl_ctx_mutex wlcl_ctx_head;
extern char mvm_ip[IP_STR_LENGTH];
extern unsigned char cvm_ip[IPV4_LEN];

/*******************************************************************************
 * @function   : bootstrap_thread
 * @brief      : Bootstrap Server socket will be created and will listen
 *               for incoming AP connections
 * @input      : none
 * @output     : none
 * @return     : none
 ******************************************************************************/

void  *bootstrap_thread(void *argv)
{
    int bootstrap_sd = 0, ap_client_sock = 0;
    struct sockaddr_in serv_addr;
    int addrlen = sizeof(serv_addr);
    int opt = 1, recv_size = 0;
    unsigned char *tmp_mac, *tmp_ip;

    bootstrapResp *responseMsg;
    ap_info_t *ap_details;

    if(!(ap_details = malloc(sizeof(ap_info_t)))) {
        app_log (LOG_CRIT, "malloc ap_details:%s", strerror(errno));
        pthread_exit(NULL);    /* Exits if memory alloc fails */
    }

    if(!(responseMsg = malloc(sizeof(bootstrapResp)))) {
        app_log (LOG_CRIT, "malloc responseMsg:%s", strerror(errno));
        free(ap_details);
        pthread_exit(NULL);    /* Exits if memory alloc fails */
    }

    /* Allocate memory for Request & Response Message Element*/

    if ((bootstrap_sd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        app_log (LOG_CRIT, "socket bootstrap_sd:%s", strerror(errno));
        close(bootstrap_sd);
        free(ap_details);
        free(responseMsg);
        pthread_exit(NULL);    /* Exits if socket desc creation fails */
    }
    memset(&serv_addr, 0, sizeof(serv_addr));
    if (setsockopt(bootstrap_sd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                &opt, sizeof(opt))) {
        app_log (LOG_CRIT, "setsockopt: %s", strerror(errno));
        close(bootstrap_sd);
        free(ap_details);
        free(responseMsg);
        pthread_exit(NULL);    /* Exits if setsockopt fails */
    }
    /* Set the socket parameters */
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(BOOTSTRAP_PORT);
    serv_addr.sin_addr.s_addr = inet_addr(mvm_ip);
    if (bind(bootstrap_sd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        app_log (LOG_CRIT, "bind bootstrap_sd: %s", strerror(errno));
        close(bootstrap_sd);
        free(ap_details);
        free(responseMsg);
        pthread_exit(NULL);    /* Exits when bind to the address failures */
    }
    if (listen(bootstrap_sd, MAX_CONN_BACKLOG) < 0) {
        app_log (LOG_CRIT, "listen bootstrap_sd: %s", strerror(errno));
        close(bootstrap_sd);
        free(ap_details);
        free(responseMsg);
        pthread_exit(NULL);    /* Exits when listen to the port failures */
    }
    RUN_FOREVER { /* Socket runs infinitely */
        /* Accept the incoming clients */
        app_log (LOG_DEBUG, "Listening on port %d", BOOTSTRAP_PORT);
        if ((ap_client_sock = accept(bootstrap_sd, (struct sockaddr *)&serv_addr,
                        (socklen_t*)&addrlen))<0) {
            app_log (LOG_NOTICE, "accept bootstrap_sd %s", strerror(errno));
            continue; /* Close the connection & continue if socket damaged */
        }
        /* Read the discovery message from the AP */
        memset(ap_details, 0, sizeof(*ap_details));
        memset(responseMsg, 0, sizeof(*responseMsg));

        recv_size = recv(ap_client_sock, ap_details, sizeof(ap_info_t), 0);
        if(recv_size == -1)
        {
            app_log (LOG_NOTICE, "recv ap_client_sock %s", strerror(errno));
            close(ap_client_sock);
            continue; /* Close the connection & continue if read failures*/
        } else if(recv_size == 0) {
            app_log (LOG_NOTICE, "recv ap_client_sock %s", strerror(errno));
            close(ap_client_sock);
            continue; /* Close the connection & continue if read is 0 bytes */
        }

        tmp_mac = ap_details->ap_mac;
        tmp_ip =  ap_details->ap_ip;

        app_log (LOG_ALERT, "Got Pre-Discover request from AP\nAP Name : %s\n Model : %s\n Mac address is %02x:%02x:%02x:%02x:%02x:%02x\nIP address is %d.%d.%d.%d\n", ap_details->ap_name, ap_details->ap_model, tmp_mac[0],
                tmp_mac[1] ,tmp_mac[2], tmp_mac[3],
                tmp_mac[4], tmp_mac[5], tmp_ip[0], tmp_ip[1],
                tmp_ip[2], tmp_ip[3]);

        if(is_ap_registered(ap_details) != true ||   /* Check for AP registered or not */
                mark_ap_bootstrapped(ap_details->ap_mac) == -1 || /* Add AP to bootstrapped list */
                getCVMIP(responseMsg) == -1) {  /* Get CVM IP */
            memset(responseMsg->cvm_ip, 0, IPV4_LEN);
			app_log (LOG_ERR, "Load balancing failed %02x:%02x:%02x:%02x:%02x:%02x\n", \
					tmp_mac[0] ,tmp_mac[1], tmp_mac[2],
					tmp_mac[3], tmp_mac[4], tmp_mac[5]);
        } else {
			app_log (LOG_INFO, "Load balance Chosen CVM IP is %d.%d.%d.%d for MAC: %02x:%02x:%02x:%02x:%02x:%02x\n", cvm_ip[0], cvm_ip[1] ,
					cvm_ip[2], cvm_ip[3], \
					tmp_mac[0] ,tmp_mac[1], tmp_mac[2], \
					tmp_mac[3], tmp_mac[4], tmp_mac[5]);
		}

        /* Send Discovery Response to the AP */
        if(reply_to_client(ap_client_sock, responseMsg, sizeof(bootstrapResp)) < 0) {
			app_log (LOG_NOTICE, "send ap_client_sock: %s", strerror(errno));
		}
        close(ap_client_sock);
    }
        close(bootstrap_sd);
        free(ap_details);
        free(responseMsg);
	app_log (LOG_CRIT, "Thread Exit: %s", __func__);
    return SUCCESS;
}

/*******************************************************************************
 * @function    : is_ap_registered
 * @brief       : This function will send AP info to DB to check AP is registered
 *                or not, if not registered - store AP info in Unregistered list
 * @input       : ap_details - Request message containing AP info
 * @output      : none
 * @return      : Status of Registration check
 *                bool true - Registered AP
 *                bool false - Unregistered AP
 ******************************************************************************/

bool is_ap_registered (ap_info_t *ap_details) {
/*Cassandra API's to be integrated*/
    int ap_reg = 0, ap_unreg = 0;

    app_log (LOG_DEBUG, "Checking AP %02x:%02x:%02x:%02x:%02x:%02x status", ap_details->ap_mac[0], ap_details->ap_mac[1], \
			ap_details->ap_mac[2], ap_details->ap_mac[3], \
			ap_details->ap_mac[4], ap_details->ap_mac[5]);
    ap_reg = cass_check_is_ap_online(ap_details->ap_mac); /* Check for AP registered */
    if(ap_reg == AP_REGISTERED) {    /* Registered but not online */
        return true;  /* Returns true if registered */
    } else if(ap_reg == AP_IS_ONLINE) { /* AP is online */
		app_log (LOG_DEBUG, "AP %02x:%02x:%02x:%02x:%02x:%02x is already online.... Invalid bootstrap request", \
				ap_details->ap_mac[0], ap_details->ap_mac[1], \
				ap_details->ap_mac[2], ap_details->ap_mac[3], \
				ap_details->ap_mac[4], ap_details->ap_mac[5]);
		return false;
    }
    else if(ap_reg == AP_NOT_REGISTERED) { /* AP is not registered */
    app_log (LOG_DEBUG, "AP is not registerd %02x:%02x:%02x:%02x:%02x:%02x", ap_details->ap_mac[0], ap_details->ap_mac[1], \
			ap_details->ap_mac[2], ap_details->ap_mac[3], \
			ap_details->ap_mac[4], ap_details->ap_mac[5]);
        ap_unreg = cass_check_ap_presence_unreg(ap_details->ap_mac);
        if(ap_unreg == AP_NOT_IN_UNREG) { /* AP info not present in unregistered list */
            cass_in_insert_ap_to_unreg(ap_details);
        } else if(ap_unreg == AP_FOUND_IN_UNREG){ /* AP info already present */
			app_log (LOG_DEBUG, "AP %02x:%02x:%02x:%02x:%02x:%02x Info already present in Unregistered list", \
					ap_details->ap_mac[0], ap_details->ap_mac[1], \
					ap_details->ap_mac[2], ap_details->ap_mac[3], \
					ap_details->ap_mac[4], ap_details->ap_mac[5]);
        } else {
			wlc_cass_print_error(ap_unreg);
        }

    } else {
        app_log (LOG_ERR, "Reading from database failed");
    }

    return false;  /* Returns false if unregistered */
}

/*******************************************************************************
 * @function    : getCVMIP
 * @brief       : This function will return the load balanced CVM IP to the
 *                calling function
 * @input       : responseMsg
 * @output      : Writes the CVM IP in the responseMsg element
 * @return      : Status of getting CVM IP after load balancing
 *                0 - Successfully got CVM IP
 *                1 - Failed to get CVM IP
 ******************************************************************************/

int getCVMIP (bootstrapResp *responseMsg) {
    int status = FAILURE;

    memcpy(responseMsg->cvm_ip, cvm_ip, sizeof(cvm_ip));
    status = SUCCESS;  /* successfully Got CVM IP */
	return status;
}

/*******************************************************************************
 * @function    : mark_ap_bootstrapped
 * @brief       : This function will store the AP Mac address in the bootstrapped
 *                linked list which could be used by WCM for bootstrap verification
 *                for the AP
 * @input       : ap_details - Request message containing AP info
 * @output      : none
 * @return      : Status of Bootstrap node creation for AP
 *                0 - success
 *                -1 - failure
 ******************************************************************************/

int mark_ap_bootstrapped(unsigned char *ap_mac) {

    struct bootstrap* bootstrap_node = NULL;
    struct list_head *loop_node = NULL;
    struct bootstrap* current = NULL;
	int ap_already_bootstrapped = -1;

    if(wlcl_block_lock_mutex(&wlcl_ctx_head) != WLCL_SUCCESS) { /* Acquire mutex lock */
        app_log (LOG_ERR, "acquire lock wlcl_ctx_head failed");
        return FAILURE;
    }
    list_for_each(loop_node, &(wlcl_ctx_head.head)) {   /* Check AP already bootstrapped */
        current = list_entry(loop_node, bootstrap_t, head_ptr);
        if(current != NULL) {
			ap_already_bootstrapped = memcmp(current->mac, ap_mac, MAC_LEN);
			if(ap_already_bootstrapped == 0) {
				app_log (LOG_DEBUG, "AP %02x:%02x:%02x:%02x:%02x:%02x is already bootstrapped", current->mac[0],
                    current->mac[1], current->mac[2],
                    current->mac[3], current->mac[4], current->mac[5]);
				break;
			}
        }
    }
    wlcl_unlock_mutex(&wlcl_ctx_head);
    if(ap_already_bootstrapped != 0) {
		bootstrap_node = calloc(1, sizeof(bootstrap_t));
		if(bootstrap_node != NULL) {
			INIT_LIST_HEAD(&bootstrap_node->head_ptr);  /* Initialize the Node prev & next ptrs */
			memcpy(bootstrap_node->mac, ap_mac, 6);
			if(wlcl_add_front_block_mutex(bootstrap_node, &wlcl_ctx_head) != SUCCESS) {
				app_log (LOG_ERR, "adding bootstrap MAC %02x:%02x:%02x:%02x:%02x:%02x failed", \
						bootstrap_node->mac[0] ,bootstrap_node->mac[1], bootstrap_node->mac[2], \
						bootstrap_node->mac[3], bootstrap_node->mac[4], bootstrap_node->mac[5]);
				free(bootstrap_node);
				return FAILURE;
			}
		}
	}
	/* Print the Bootstrapped AP list */
#ifdef WCM_DEBUG_LOG
    print_bootstrap_list();
#endif
	return SUCCESS;
}

/*******************************************************************************
 * @function    : print_bootstrap_list
 * @brief       : This function will print the available APs under bootstrapped
 * 				  linked list
 * @input       : none
 * @output      : Bootstrapped list
 * @return      : none
 ******************************************************************************/

void print_bootstrap_list()
{
    struct list_head *loop_node = NULL;
    struct bootstrap* current = NULL;
	if(wlcl_block_lock_mutex(&wlcl_ctx_head) != WLCL_SUCCESS) { /* Acquire mutex lock */
        app_log (LOG_ERR, "acquire lock wlcl_ctx_head failed");
        return;
    }

    app_log (LOG_DEBUG, "APs under bootstrapped list are ");
    list_for_each(loop_node, &(wlcl_ctx_head.head)) {   /* Print the Bootstrapped APs */
        current = list_entry(loop_node, bootstrap_t, head_ptr);
        if(current != NULL) {
            app_log (LOG_DEBUG, "%02x:%02x:%02x:%02x:%02x:%02x\n", current->mac[0],
                    current->mac[1], current->mac[2],
                    current->mac[3], current->mac[4], current->mac[5]);
        }
    }
    wlcl_unlock_mutex(&wlcl_ctx_head);
}
