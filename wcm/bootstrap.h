#ifndef __BOOTSTRAP_H_INCLUDE__
#define __BOOTSTRAP_H_INCLUDE__
#include <stdbool.h>
#include "wlc_gen_macro.h"
#include "wlc_ap_info_struct.h"
#define CVM_IP  {192, 168, 101, 186} /* CVM IP */ /* TODO MVM will select IP after load balance*/
#define MAX_CONN_BACKLOG 42949672 /* Max. length to which pending connection for sockfd can grow */
#define BUF_SIZE 1024   /* Buffer size */
#define AP_NOT_REGISTERED -2 /* Macro for unregistered status */
#define AP_REGISTERED -6   /* Macro for registered status */
#define AP_IS_ONLINE 0   /* Macro for AP online status */
#define AP_NOT_IN_UNREG -2 /* Macro for AP not present in unregistered list */
#define AP_FOUND_IN_UNREG 0    /* Macro for AP present in unregistered list */
/* Macro for dynamic memory allocation */
#define RUN_FOREVER while(1)    /* While loop */


/*******************************************************************************
 * @structure   : bootstrapResp
 * @brief       : This structure contains the packet format for discovery response
 * @members     :
 *  @cvmIP      : IP address of the Compute VM
 ******************************************************************************/

typedef struct{
    unsigned char cvm_ip[IPV4_LEN];
}__attribute__((packed)) bootstrapResp;

/* Function Declarations */

/*******************************************************************************
 * @function   : bootstrap_thread
 * @brief      : Bootstrap Server socket will be created and will listen
 *               for incoming AP connections
 * @input      : none
 * @output     : none
 * @return     : none
 ******************************************************************************/

void *bootstrap_thread(void *);

/*******************************************************************************
 * @function    : getCVMIP
 * @brief       : This function will return the load balanced CVM IP to the
 *                calling function
 * @input       : responseMsg
 * @output      : Writes the CVM IP in the responseMsg element
 * @return      : Status of getting CVM IP after load balancing
 *                0 - Successfully got CVM IP
 *                1 - Failed to get CVM IP
 ******************************************************************************/

int getCVMIP (bootstrapResp *responseMsg);

/*******************************************************************************
 * @function    : is_ap_registered
 * @brief       : This function will send AP info to DB to check AP is registered
 *                or not, if not registered - store AP info in Unregistered list
 * @input       : ap_details - Request message containing AP info
 * @output      : none
 * @return      : Status of Registration check
 *                bool true - Registered AP
 *                bool false - Unregistered AP
 ******************************************************************************/

bool is_ap_registered (ap_info_t *ap_details);

/*******************************************************************************
 * @function    : mark_ap_bootstrapped
 * @brief       : This function will store the AP Mac address in the bootstrapped
 *                linked list which could be used by WCM for bootstrap verification
 *                for the AP
 * @input       : ap_details - Request message containing AP info
 * @output      : none
 * @return      : Status of Bootstrap node creation for AP
 *                0 - success
 *                -1 - failure
 ******************************************************************************/

int mark_ap_bootstrapped(unsigned char *mac);

/*******************************************************************************
 * @function    : print_bootstrap_list
 * @brief       : This function will print the available APs under bootstrapped
 * 				  linked list
 * @input       : none
 * @output      : Bootstrapped list
 * @return      : none
 ******************************************************************************/

void print_bootstrap_list();

#endif
