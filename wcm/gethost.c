#include <stdio.h>
#include <netdb.h>


int main(int argc, char *argv[])
{
	int i;
    struct hostent *lh = gethostbyname("localhost");
	unsigned char ip_addr[4];
    if (lh)
        puts(lh->h_name);
    else
        herror("gethostbyname");
	printf("IP len:%d\n", lh->h_length);
	memcpy(ip_addr, lh->h_addr_list[0], lh->h_length);
	for(i=0; i<4; i++) {
		printf("IP[%d] is %d\n", i, ip_addr[i]);
	}
    return 0;
}
