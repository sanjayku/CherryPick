/*******************************************************************************
 * @file        : preDiscover_client.c
 * @brief       : This source file contains code for creating socket with MVM
 *                to get the IP address of the available CVM and returns it
 *                to the WTP instance.
 * @author      : K Arun Pandi (arunpandi.k@vvdntech.in)
 * @copyright   : (c) 2017-2018, VVDN Technologies Pvt. Ltd.
 *                   Permission is hereby granted to everyone in VVDN Technologies
 *                   to use the Software without restriction, including without
 *                   limitation the rights to use, copy, modify, merge, publish,
 *                   distribute, distribute with modifications.
 ******************************************************************************/

#include "preDiscover_client.h"
#include "../../inc/wlc_ap_info_struct.h"
#define MVM_IP "192.168.102.17"
/*******************************************************************************
* @function		: main
* @param		: void
* @return 		: success/failure(0/-1)
* @return_type 	: int
* @brief 		: Main function will execute all the modules and returns
                    success/failure
 ******************************************************************************/

int main(int argc, char const *argv[])
{
    int sock = 0;
    struct sockaddr_in serv_addr;
    int ipLen = 0;
    unsigned char *macAddr;
    //unsigned char formattedMAC[MAC_LEN];
	/*char *formattedMAC = "38:59:f9:1c:41:f5";*/
	/*char *formattedMAC = "74:ea:3a:91:9b:8a";*/
    char *formattedMAC = "14:fe:b5:b8:86:70";
    char formattedIP[] = "192.168.100.185";
    char* buff;
    int i = 0;
	ap_info_t requestMsg;
   // bootstrapReq requestMsg;
    bootstrapResp *responseMsg;
    //bootstrapReq *requestMsg;
    /* Allocate memory for discovery Response pointer */
    ALLOC_MEM(responseMsg, sizeof(bootstrapResp));
    //ALLOC_MEM(requestMsg, sizeof(bootstrapReq));
    do {
    i = 0;
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            printf("\n Socket creation error \n");
            return FAILURE;
        }
        bzero(requestMsg.ap_mac, MAC_LEN);
        //memset(&serv_addr, '0', sizeof(serv_addr));
        /* Set the socket parameters */
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(PORT);
        /* Convert IPv4 and IPv6 addresses from text to binary form */
        if(inet_pton(AF_INET, MVM_IP, &serv_addr.sin_addr) <= 0)
        {
            printf("\nInvalid address/ Address not supported \n");
            return FAILURE;
        }
        if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        {
            printf("\nConnection Failed \n");
            return FAILURE;
        }
        /*if (getLocalMACAddr(&macAddr) != SUCCESS) {
          printf("Interface does not exists\n");
          return FAILURE;
          }
        //Formatting MAC address
        sprintf(formattedMAC, "%02X:%02X:%02X:%02X:%02X:%02X",
        (macAddr[0] & 0xff), (macAddr[1] & 0xff), (macAddr[2] & 0xff),
        (macAddr[3] & 0xff), (macAddr[4] & 0xff), (macAddr[5] & 0xff));
        */
        memset(&requestMsg, 0, sizeof(ap_info_t));
        memset(responseMsg, 0, sizeof(*responseMsg));

        strcpy(formattedIP, "192.168.100.181");

        buff = malloc(sizeof(char) * 10);
        bzero(buff, sizeof(buff));
        buff = strtok(formattedIP,".");
        while (buff != NULL)
        {
            requestMsg.ap_ip[i] = (unsigned char)atoi(buff);
            buff = strtok(NULL,".");
            i++;
        }
        free(buff);
        memcpy(requestMsg.ap_name, "AP1", AP_NAME_LEN);
		/*requestMsg.ap_model[0] = '2';
		requestMsg.ap_model[1] = '5';
		requestMsg.ap_model[2] = '2';
		//requestMsg.ap_model[3] = '\0';
	 */
	 	memcpy(requestMsg.ap_model, "252", sizeof(requestMsg.ap_model));
		memset(requestMsg.ap_mac, 0, MAC_LEN);
		sscanf(formattedMAC, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
				&(requestMsg.ap_mac[0]), &(requestMsg.ap_mac[1]), &(requestMsg.ap_mac[2]),
				&(requestMsg.ap_mac[3]), &(requestMsg.ap_mac[4]), &(requestMsg.ap_mac[5]) );
		//strcpy(requestMsg.ap_model, "15");
		//memcpy(requestMsg.ap_model, "678", MODEL_NUM_LEN);
		//printf("The length of mac %ld, size %ld, and ip %ld, size %ld\n", strlen(requestMsg.ap_mac), sizeof(requestMsg.ap_mac), strlen(requestMsg.ap_ip), sizeof(requestMsg.ap_ip));
		printf("Name is %s\n", requestMsg.ap_name);
		printf("Model is %s\n", requestMsg.ap_model);
		printf("Mac address is ");
		for(i = 0; i < MAC_LEN; i++) {
			printf("%02x", requestMsg.ap_mac[i]);
			if(i != (MAC_LEN -1)) printf(":");
		}
		printf("\n");
		printf("IP address is ");
		for(i = 0; i < IP_LEN; i++) {
			printf("%d", requestMsg.ap_ip[i]);
			if(i != (IP_LEN -1)) printf(".");
		}
		printf("\n");

		send(sock, &requestMsg, sizeof(ap_info_t), 0);
		/* Send Discovery Request */
		//send(sock, formattedMAC, strlen(formattedMAC), 0);
		printf("Discovery Request sent. Waiting for CVM IP \n");

		/* Parse the Discovery Response */
		if (Readn(sock, responseMsg, sizeof(bootstrapResp)) != sizeof(bootstrapResp)) {
			printf("Unable to parse the discovery response\n");
		}
		if(!strcmp(responseMsg->cvm_ip, DUMMY_IP)) {   /* Success Response */
			printf("Mac not registered in DB\n");
			sleep(10);  /* Retry after some time */
			printf("Resending the discovery Request\n");

		} else {
			printf("Discovery Response received\nConnecting to CVM IP ");
			for(i = 0; i < IP_LEN; i++) {
				printf("%d", responseMsg->cvm_ip[i]);
				if(i != (IP_LEN -1)) printf(".");
			}
			printf("\n");
			/*if(registerCVMIPtoWTP(responseMsg) != SUCCESS) {
			  printf("Can't use CVM IP for CAPWAP instance");
			  }*/

			break;
		}
		close(sock);
		//    bzero(requestMsg.ap_ip, sizeof(requestMsg.ap_ip));
		//memset(requestMsg.ap_ip, '0', IP_LEN);

	} while (1);  /* Resend Request if failure resp */
	return SUCCESS;
}

/*******************************************************************************
 * @function	: getLocalMACAddr
 * @param1 		: macAddr
 * @return 		: success/failure(0/-1)
 * @return_type : int
 * @brief 		: Gets the MAC address of the WTP interface
 ******************************************************************************/

int getLocalMACAddr(unsigned char** macAddr) {

	struct ifreq ethreq;
	int sock2 = 0;
	char *interfaceName = NULL;
	// creating second socket for get Local MAC address
	sock2 = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock2 < 0) {
		printf("Error Creating Socket for ioctl");
		return FAILURE;
	}
	memset(&ethreq, 0, sizeof(ethreq));
	interfaceName = getInterfaceName();
	strncpy(ethreq.ifr_name, interfaceName, IFNAMSIZ);
	if (ioctl(sock2, SIOCGIFHWADDR, &ethreq) == -1) {
		return FAILURE;
	}
	*macAddr = (unsigned char *)ethreq.ifr_hwaddr.sa_data;
	return SUCCESS;
}


/***************************************************************
 * @function	: registerCVMIPtoWTP
 * @param1 		: preDiscover
 * @return 		: Success or failure
 * @return_type : int
 * @brief 		: Sends CVMIP to WTP
 ***************************************************************/

int registerCVMIPtoWTP(bootstrapResp *responseMsg) {
	char *acAddr = "<AC_ADDRESSES>";
	char *str = NULL;
	size_t len = 0;
	FILE *CONFIG_FILE, *TMP_FILE;

	if((CONFIG_FILE= fopen("config.wtp", "r+")) < 0) {
		printf("unable to open file");
		return FAILURE;
	}
	TMP_FILE = tmpfile();
	while((getline(&str, &len, CONFIG_FILE)) != -1) {
		if(!strstr(str, acAddr)) {
			fprintf(TMP_FILE, "%s", str);
		} else {
			fprintf(TMP_FILE, "%s", str);
			fprintf(TMP_FILE, "%s", responseMsg->cvm_ip);
			fputc('\n', TMP_FILE);
			getline(&str, &len, CONFIG_FILE);
		}
	}
	fclose(CONFIG_FILE);
	fseek(TMP_FILE, 0, 0);
	if((CONFIG_FILE= fopen("config.wtp", "w")) < 0) {
		printf("unable to open file");
		return FAILURE;
	}
	while((getline(&str, &len, TMP_FILE)) != FAILURE) {
		fprintf(CONFIG_FILE, "%s", str);
	}
	fclose(TMP_FILE);
	fclose(CONFIG_FILE);
	return SUCCESS;
}

/***************************************************************
 * @function	: getInterfaceName
 * @param 		: void
 * @return 		: returns the interface name
 * @return_type : char
 * @brief 		: Gets the WTP interface name
 ***************************************************************/

char* getInterfaceName() {
	char *IFname = "<WTP_ETH_IF_NAME>";
	char str[BUF_SIZE], *buffer;
	FILE *CONFIG_FILE;

	if((CONFIG_FILE= fopen("settings.wtp.txt", "r+")) < 0) {
		printf("unable to open file\n");
	}
	while(!(feof(CONFIG_FILE))) {
		fscanf(CONFIG_FILE, "%s", str );
		if(strstr(str, IFname) != 0) {
			if(strcmp(str, IFname) == 0) {
				fscanf(CONFIG_FILE, "%s", str);
				buffer = malloc(strlen(str));
				strcpy(buffer, str);
			} else {
				buffer = malloc(strlen(str) - strlen(IFname));
				strcpy(buffer,str+strlen(IFname));
			}
			while(isspace(*buffer))
				buffer++;
			break;
		}
	}
	return buffer;
}

/*******************************************************************************
 * @function	: readn
 * @param1      : socket descriptor sock_desc
 * @param2      : structure vptr
 * @param3      : size nbytes
 * @return      : number of bytes received
 * @return_type : int
 * @brief 		: Receive the discovery response from MVM
 ******************************************************************************/

int readn(int fd, void *vptr, size_t nbytes)
{
	size_t  nleft;
	ssize_t nread;
	char    *ptr;

	ptr = vptr;
	nleft = nbytes;
	while (nleft > 0) {
		if ( (nread = recv(fd, ptr, nleft, 0)) < 0) {
			if (errno == EINTR)
				nread = 0;      /* and call read() again */
			else
				return(-1);
		} else if (nread == 0)
			break;              /* EOF */

		nleft -= nread;
		ptr   += nread;
	}
	return(nbytes - nleft);      /* return >= 0 */
}

/*******************************************************************************
 * @function	: readn
 * @param1      : socket descriptor sock_desc
 * @param2      : structure vptr
 * @param3      : size nbytes
 * @return      : success/failure (0/1)
 * @return_type : int
 * @brief 		: Sub routine for reading Discovery Response
 ******************************************************************************/

int Readn(int fd, void *ptr, size_t nbytes)
{
	int n = 0;
	if ( (n = readn(fd, ptr, nbytes)) < 0)
		perror("readn error");
	return(n);
}

/*******************************************************************************
 * @function	: readn
 * @param1      : socket descriptor sock_desc
 * @param2      : structure vptr
 * @param3      : size nbytes
 * @return      : success/failure (0/1)
 * @return_type : int
 * @brief 		: Sub routine for reading Discovery Response
 ******************************************************************************/

int Read(int fd, int *ptr)
{
	int ret;

	ret = Readn(fd, ptr, 4);

	*ptr = ntohl(*ptr);

	return ret;
}
