#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <ctype.h>
#define PORT 8520
#define WCM_IP "127.0.0.1"
#define SUCCESS 0
#define FAILURE -1
#define IP_LEN 4
#define MAC_LEN 6
#define DUMMY_IP "0000"
#define AP_NAME_LEN 32
//#define MODEL_NUM_LEN 3 
#define BUF_SIZE 1024
#define UNREG 2
#define ALLOC_MEM(varName, size) { varName = malloc(size); if(!varName) MEM_ERR}
#define MEM_ERR printf("Memory allocation error\n");

/*******************************************************************************
 * @structure   : discoveryResp
 * @brief       : This structure contains the packet format for discovery response
 * @members     :
 *  @cvmIP      : IP address of the Compute VM
 *  @result_code: Result code (Success/Failure)
 ******************************************************************************/


/*typedef struct {
    char ap_name[AP_NAME_LEN+1];
    char ap_model[MODEL_NUM_LEN+1];
    unsigned char ap_mac[MAC_LEN];
    unsigned char ap_ip[IP_LEN];
}bootstrapReq;
*/
typedef struct {
    unsigned char cvm_ip[IP_LEN];
}__attribute__((packed)) bootstrapResp;


int getLocalMACAddr(unsigned char**);
char* getInterfaceName();
int readn(int fd, void *vptr, size_t n);
int Read(int fd, int *ptr);
int registerCVMIPtoWTP(bootstrapResp *responseMsg );
int Readn(int fd, void *ptr, size_t nbytes);

