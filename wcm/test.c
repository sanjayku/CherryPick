
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include "../../inc/wlcl.h"
#define SUCCESS 0
#define FAILURE 1
#define WCM_FAIL 1
#define WCM_SUCCESS 0
#define MAC_LEN 6
struct wlcl_ctx_mutex wlcl_ctx_head;/*TODO: extern it from bootstrap*/

typedef struct bootstrap {/*TODO: shift to structure*/
    struct list_head head_ptr;
    int id;
    unsigned char mac[6];
}bootstrap_t;

int delete_mac(unsigned char *mac)
{
	int i, ret;
	struct bootstrap* bootstrap_node = NULL;
	struct list_head *loop_node = NULL;
	struct bootstrap* current = NULL;
	
	if(wlcl_block_lock_mutex(&wlcl_ctx_head) != SUCCESS) {
		printf("failed acquiring lock\n");
		return FAILURE;
	}

	list_for_each(loop_node, &(wlcl_ctx_head.head)) {
		current = list_entry(loop_node, struct bootstrap, head_ptr);
		if(NULL != current) {
			ret = memcmp(current->mac, mac, MAC_LEN);
			if(ret == 0) {
				printf("\nmac found\n");
				wlcl_del_node(current);/*Deleted node*/
				/*Mutex unlock*/
				wlcl_unlock_mutex(&wlcl_ctx_head);
				return WCM_SUCCESS;/*Set success status to send bootstrap success*/
			}

#if 0
			for(i=0; i<MAC_LEN; i++) {
				if(current->mac[i] == mac[i]) {
					continue;
				} else {
					break;
				}
			}
			if(i == MAC_LEN) {
				printf("\nmac found\n");
				wlcl_del_node(current);
				wlcl_unlock_mutex(&wlcl_ctx_head);
				return WCM_SUCCESS;
			}
#endif
		}
	}
	printf("\nMac not found\n");
	wlcl_unlock_mutex(&wlcl_ctx_head);
	return WCM_FAIL;
}

int main()
{
	int ret, i, ret_val = 0;
	bootstrap_t *node1;
	bootstrap_t *node2;
	struct list_head *loop_node = NULL;
	struct bootstrap* current = NULL;
	unsigned char mac[6];
	mac[0] = 5;
	mac[0] = 4;
	mac[0] = 3;
	mac[0] = 2;
	mac[0] = 1;
	mac[0] = 0;

    if (setup_wlcl_mutex(&wlcl_ctx_head) != 0) { /* Acquiring Mutex lock to the List */
        printf("Can't acquire mutex lock\n");
        return WCM_FAIL;
    }
	/*Fill node1*/
	node1 = malloc(sizeof(bootstrap_t));
	if(node1 == NULL) {
		printf("\nMalloc failed\n");
		goto END;
	}
	node1->id = 1;
	for(i=0;i<6;i++) {
		node1->mac[i] = i;
	}
	/*Fill node2*/
	node2 = malloc(sizeof(bootstrap_t));
	if(node2 == NULL) {
		printf("\nMalloc failed\n");
		goto END;
	}
	node2->id = 1;
	for(i=0;i<6;i++) {
		node2->mac[i] = i;
	}

	if(wlcl_add_end_block_mutex(node1, &wlcl_ctx_head) != SUCCESS) {
		printf("adding element failed\n");
		return FAILURE;
	}

	if(wlcl_add_end_block_mutex(node2, &wlcl_ctx_head) != SUCCESS) {
		printf("adding element failed\n");
		return FAILURE;
	}

	if(wlcl_block_lock_mutex(&wlcl_ctx_head) != SUCCESS) {
		printf("failed acquiring lock\n");
		return FAILURE;
	}
	list_for_each(loop_node, &(wlcl_ctx_head.head)) {
		current = list_entry(loop_node, struct bootstrap, head_ptr);
		if(NULL != current) {
			for(i=0;i<6;i++) {
				printf("Mac[%d]: %d", i, node1->mac[i]);
			}
		}
	}
	wlcl_unlock_mutex(&wlcl_ctx_head);
	ret = delete_mac(mac);
	if(ret == 1) {
		printf("\nmac not found n0\n");
	}	
	ret = delete_mac(node1->mac);
	if(ret == 1) {
		printf("\nmac not found n1\n");
	}	
	ret = delete_mac(node2->mac);
	if(ret == 1) {
		printf("\nmac not foundn n2\n");
	}	
	ret = delete_mac(node2->mac);
	if(ret == 1) {
		printf("\nmac not foundn n3\n");
	}	

	if(destroy_wlcl_mutex(&wlcl_ctx_head) != 0) {
        printf("Can't delete mutex lock\n");
        return WCM_FAIL;
	}
END:
	return 0;
}
