#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include<pthread.h>
#include "wlc_common.pb-c.h"
#include "wcm_ac_common.h"
#include "wlc_common_struct.h"
#define MVM_IP "192.168.101.157"
#define THREAD_COUNT 1
pthread_mutex_t lock;
void * trythis(void *try)
{
	struct wtp_basic_details wtp_info;
	int sock_fd, ret, i;
	gcp_vap_info_t ap_change;
	common_msg_t *msg;
	common_msg_t *comm_msg_proto_recv;
	int comm_len;
	unsigned char *comm_msg_data = NULL;
	unsigned char recv_buf[2048];
	sock_fd = *((int *)try);
#if 0
	msg.msg_type = AP_BULK_CONFIG;
	msg.has_serial_bytes = 1;
	memset(&config, 0, sizeof(config));
	msg.serial_bytes.len = sizeof(config);
//0x74ea3a919b8a	
	msg.serial_bytes.data = malloc(sizeof(config));;
	config.wtp_mac_info.macaddr[0] = 0x14;
	config.wtp_mac_info.macaddr[1] = 0xfe;
	config.wtp_mac_info.macaddr[2] = 0xb5;
	config.wtp_mac_info.macaddr[3] = 0xb8;
	config.wtp_mac_info.macaddr[4] = 0x86;
	config.wtp_mac_info.macaddr[5] = 0x70;
	memcpy(msg.serial_bytes.data, &config, msg.serial_bytes.len);
	/*memcpy(msg.serial_bytes.data, 1, sizeof(config));*/
#endif
//0xddbb14882610
//0xefddcc406910
//14:fe:b5:b8:86:70
#if 0
	wtp_info.macaddr[0] = 0x14;
	wtp_info.macaddr[1] = 0xfe;
	wtp_info.macaddr[2] = 0xb5;
	wtp_info.macaddr[3] = 0xb8;
	wtp_info.macaddr[4] = 0x86;
	wtp_info.macaddr[5] = 0x70;
	wtp_info.wtp_id = 2;

	msg = malloc(sizeof(common_msg_t)+sizeof(wtp_info));
	if(msg == NULL) {
		printf("\nmalloc failed\n");
		return NULL;
	}
	msg->interface = IF_TYPE__WCM_AC_INTERFACE;
	msg->msg_type = CHECK_BOOTSTRAP;
	msg->data_len = sizeof(wtp_info);
	memcpy(msg->data, &wtp_info, msg->data_len);
	comm_len = sizeof(wtp_info)+sizeof(common_msg_t);
#endif
#if 0
	msg = malloc(sizeof(common_msg_t));
	if(msg == NULL) {
		printf("\nmalloc failed\n");
		return NULL;
	}

	msg->interface = IF_TYPE__WCM_AC_INTERFACE;
	msg->msg_type = AC_CONFIG;
	msg->data_len = 0;
	comm_len = sizeof(common_msg_t);
#endif
#if 0
	msg.msg_type = AP_STATUS;
	msg.has_serial_bytes = 1;
	memset(&config, 0, sizeof(config));
	msg.serial_bytes.len = sizeof(config.wtp_mac_info);
	
	msg.serial_bytes.data = malloc(sizeof(config.wtp_mac_info));;
//b8:2a:72:ad:85:81
//38:59:f9:1c:41:f5
//74:ea:3a:91:9b:8a
//c0:38:96:96:78:87
//34:17:eb:93:00:2a
//14:fe:b5:b8:86:e4
	config.wtp_mac_info.macaddr[0] = 0x14;
	config.wtp_mac_info.macaddr[1] = 0xfe;
	config.wtp_mac_info.macaddr[2] = 0xb5;
	config.wtp_mac_info.macaddr[3] = 0xb8;
	config.wtp_mac_info.macaddr[4] = 0x86;
	config.wtp_mac_info.macaddr[5] = 0x70;
	config.wtp_mac_info.wtp_status = 1;
	memcpy(msg.serial_bytes.data, &config.wtp_mac_info, msg.serial_bytes.len);
#endif


    printf("\ncommon message len : %d\n", comm_len);
	for(i=0; i<1; i++) {
		pthread_mutex_lock(&lock);
		ret = send(sock_fd, msg, comm_len, 0);

		if(ret > 0)
		{
			printf("\nsent:%d\n", ret);
		}
		pthread_mutex_unlock(&lock);
		pthread_mutex_lock(&lock);
		if(i%4 == 0) {
RECV:
			memset(recv_buf, 0, sizeof(recv_buf));
			ret = recv(sock_fd, recv_buf, sizeof(recv_buf), 0);
			if(ret > 0) {
				printf("\nrecv len: %d\n", ret);
				comm_msg_proto_recv = malloc(ret);
				if(comm_msg_proto_recv == NULL) {
					perror("malloc");
					continue;
				}
				memcpy(comm_msg_proto_recv, recv_buf, ret);
				printf("\ncommon msg recv: response:%d\n", comm_msg_proto_recv->response);
				memset(&wtp_info, 0, sizeof(struct wtp_basic_details));
				printf("\nrecv len: %d\n", comm_msg_proto_recv->data_len );
				memcpy(&ap_change, comm_msg_proto_recv->data, comm_msg_proto_recv->data_len);
				printf("\nvapid: %s, group id: %s radio_type:%d wlan_id:%d\n", ap_change.vapID, ap_change.groupID, ap_change.radio_type, ap_change.wlan_id);
			}
			goto RECV;
		}
		pthread_mutex_unlock(&lock);
	}
	while(1);
	free(comm_msg_data);
	return NULL;
}

int main()
{
	struct sockaddr_in srcAddr;
	int sock_fd, ret, i;
	unsigned char ip_buf[4];
	char ip_str[20];
	pthread_t tid[THREAD_COUNT];
	printf("2");	
	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (-1 == sock_fd) {
		printf ("socket creation error\r\n");
		/*return -1;*/
	}

	printf ("connecting to host: 0.0.0.0 on 7001\r\n");
	srcAddr.sin_port = htons(7001);
	srcAddr.sin_addr.s_addr = inet_addr(MVM_IP);
	srcAddr.sin_family = AF_INET;

	if (-1 == connect (sock_fd, (struct sockaddr *)&srcAddr, sizeof(srcAddr))) {
		printf ("error connecting to %s:7001\r\n", MVM_IP);
		close (sock_fd);
		/*return -1;*/
	} else {
		printf("\r\nConnected\r\n");
		memset(ip_str, 0, sizeof(ip_str));
		ret = recv(sock_fd, ip_str, sizeof(ip_str), 0);
		if(ret > 0) {
			printf("\nip_str: %s\n", ip_str);
			if(strcmp(ip_str, "GET_IP") == 0) {
				memset(ip_buf, 0, sizeof(ip_buf));
				ip_buf[0] = 192;
				ip_buf[1] = 168;
				ip_buf[2] = 103;
				ip_buf[3] = 107;
				ret = send(sock_fd, ip_buf, sizeof(ip_buf), 0);
				if(ret > 0) {
					printf("\nsent CVM IP\n");
				}
			}
		}
		if (pthread_mutex_init(&lock, NULL) != 0)
		{
			printf("\n mutex init has failed\n");
			return 1;
		}
		for(i = 0; i<THREAD_COUNT; i++) {
			ret = pthread_create(&(tid[i]), NULL, &trythis, (void *)&sock_fd);
			if (ret != 0)
				printf("\nThread can't be created : [%s]", strerror(ret));
		}
		while(1);
	}
	pthread_mutex_destroy(&lock);
	return 0;
}

