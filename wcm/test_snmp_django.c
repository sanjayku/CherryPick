#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include<pthread.h>
#include "wlc_common.pb-c.h"
#include "django_wcm.pb-c.h"
#include "snmp_wcm.pb-c.h"
#include "cli_wcm.pb-c.h"
#include "wcm_ac_common.h"
#include "wlc_common_struct.h"
#define MVM_IP "192.168.101.157"
#define FILEWRITE                                            

#define THREAD_COUNT 1
#define MSG_COUNT 1
pthread_mutex_t lock;
void * trythis(void *try) {
	struct sockaddr_in srcAddr;
	int sock_fd, ret, i;
	/*CommonMsg msg = COMMON_MSG__INIT;*/
	/*CommonMsg *comm_msg_proto_recv = NULL;*/
	sock_fd = *((int *)try);
	common_msg_t *msg;
#ifdef FILEWRITE                                                       
    FILE *fptr;         
#endif       
	VapConfig vap_msg = VAP_CONFIG__INIT;
	GroupConfig group_msg = GROUP_CONFIG__INIT;
	RegisterAP reg_msg = REGISTER_AP__INIT;
	ApDetails ap_msg = AP_DETAILS__INIT;
	UnregisterAP unreg_msg = UNREGISTER_AP__INIT;
	ImageUpgrade reboot_msg = IMAGE_UPGRADE__INIT;
	GroupSsidConfig group_ssid = GROUP_SSID_CONFIG__INIT;
	int vap_msg_len, group_msg_len, reg_msg_len, unreg_msg_len, reboot_msg_len, group_ssid_len;
	unsigned char *vap_msg_data = NULL, *group_msg_data = NULL, *reg_msg_data = NULL, *unreg_msg_data = NULL, *reboot_msg_data=NULL, *group_ssid_data;
	int comm_len, len;
	unsigned char *comm_msg_data = NULL;
	unsigned char recv_buf[2048];
	/*CREATE VAP*/
	/*vap_msg.vapid = "12616b24-2169-44de-9aff-484a2f3fc210";*/
	vap_msg.vapid = "777c889d-4245-4222-8701-bed4f44b6555";
	vap_msg.ssid = "SSIDUP3";
	vap_msg.has_authtype = 1;
	vap_msg.authtype = 1;
	vap_msg.passphrase = "password259";
	vap_msg_len = vap_config__get_packed_size(&vap_msg); 
	printf("\nvap message len : %d\n", vap_msg_len);
	vap_msg_data = malloc(vap_msg_len);
	if(vap_msg_data == NULL)
	{
		printf("\nmalloc error\n");
		goto END;
	}
	len = vap_config__pack(&vap_msg, vap_msg_data);
	if(len <=0)
	{
		printf("\npacking fail\n");
	}
	/*CREATE GROUP*/
	group_msg.groupname = "Testmanager";
	group_msg.groupid = "2079322f-8fcf-4467-9b9f-44f3e56efdef";
	group_msg_len = group_config__get_packed_size(&group_msg); 
	printf("\ngroup message len : %d\n", group_msg_len);
	group_msg_data = malloc(group_msg_len);
	if(group_msg_data == NULL)
	{
		printf("\nmalloc error\n");
		goto END;
	}
	len = group_config__pack(&group_msg, group_msg_data);
	if(len <=0)
	{
		printf("\npacking fail\n");
	}
	/*GROUP VAP MAP*/
	group_ssid.groupid = "b4deb9e0-2388-11e8-82fa-9158f51545a6";
	group_ssid.vapid = "b4df7d30-2388-11e8-a21b-9158f51545a6";
	group_ssid.radiotype = 1;
	group_ssid_len = group_ssid_config__get_packed_size(&group_ssid); 
	printf("\ngroup ssid len : %d\n", group_ssid_len);
	group_ssid_data = malloc(group_ssid_len);
	if(group_ssid_data == NULL)
	{
		printf("\nmalloc error\n");
		goto END;
	}
	len = group_ssid_config__pack(&group_ssid, group_ssid_data);
	if(len <=0)
	{
		printf("\npacking fail\n");
	}


	/*Register AP*/
	ap_msg.ap_name = "AP8";
	ap_msg.ap_model = "Model678";
	ap_msg.has_ap_mac = 1;
	ap_msg.ap_mac.len = 6;
	ap_msg.ap_mac.data = (unsigned char*)malloc(6 * sizeof(unsigned char));
	if(ap_msg.ap_mac.data == NULL) {
		printf("\nmalloc error\n");
		goto END;
	}
	//38:59:f9:1c:41:f5
//74:ea:3a:91:9b:8a
//c0:38:96:96:78:87
//34:17:eb:93:00:2a
//14:fe:b5:b8:86:e4
//de:ff:cc:60:10:34
//0xefddcc406910
	ap_msg.ap_mac.data[0] = 0x10;
	ap_msg.ap_mac.data[1] = 0x10;
	ap_msg.ap_mac.data[2] = 0x10;
	ap_msg.ap_mac.data[3] = 0x10;
	ap_msg.ap_mac.data[4] = 0x10;
	ap_msg.ap_mac.data[5] = 0x01;
	reg_msg.ap_info = &ap_msg;
	reg_msg.groupname = group_msg.groupname;
	reg_msg.groupid = group_msg.groupid;

	reg_msg_len = register_ap__get_packed_size(&reg_msg); 
	printf("\nreg message len : %d\n", reg_msg_len);
	reg_msg_data = malloc(reg_msg_len);
	if(reg_msg_data == NULL)
	{
		printf("\nmalloc error\n");
		goto END;
	}
	len = register_ap__pack(&reg_msg, reg_msg_data);
	if(len <=0)
	{
		printf("\npacking fail\n");
	}

	/*Unregister AP*/
	unreg_msg.mac.len = 6;
	unreg_msg.mac.data = (unsigned char*)malloc(6*sizeof(unsigned char));
	if(unreg_msg.mac.data == NULL) {
		printf("\nmalloc error\n");
		goto END;
	}
	for(i=0; i<unreg_msg.mac.len; i++) {
		unreg_msg.mac.data[i] = ap_msg.ap_mac.data[i];
	}
	unreg_msg_len = unregister_ap__get_packed_size(&unreg_msg); 
	printf("\nreg message len : %d\n", unreg_msg_len);
	unreg_msg_data = malloc(unreg_msg_len);
	if(unreg_msg_data == NULL)
	{
		printf("\nmalloc error\n");
		goto END;
	}
	len = unregister_ap__pack(&unreg_msg, unreg_msg_data);
	if(len <=0)
	{
		printf("\npacking fail\n");
	}
	/*Image upgrade*/
	reboot_msg.filepath = "/tmp/wlc-images/";
	reboot_msg.filename = "wlc_bin_0.0.21.tar.gz";
	reboot_msg_len = image_upgrade__get_packed_size(&reboot_msg); 
	printf("\nreboot message len : %d\n", reboot_msg_len);
	reboot_msg_data = malloc(reboot_msg_len);
	if(reboot_msg_data == NULL)
	{
		printf("\nmalloc error\n");
		goto END;
	}
	len = image_upgrade__pack(&reboot_msg, reboot_msg_data);
	if(len <=0)
	{
		printf("\npacking fail\n");
	}

#if 0//reboot
	msg = malloc(sizeof(common_msg_t) + reboot_msg_len);
	if(msg == NULL) {
		perror("malloc");
		goto END;
	}
	msg->interface = IF_TYPE__WCM_DJANGO_INTERFACE;
	msg->msg_type = MSG_TYPES__DJANGO_IMG_UPGRADE_REQ;
	msg->data_len = reboot_msg_len;
	memcpy(msg->data, reboot_msg_data, reboot_msg_len);
	comm_len = sizeof(common_msg_t) + reboot_msg_len;
	printf("\nmsg->data[0]:%d\n", msg->data[0]);
#endif


#if 1//Unregister AP
	msg = malloc(sizeof(common_msg_t) + unreg_msg_len);
	if(msg == NULL) {
		perror("malloc");
		goto END;
	}
	msg->interface = IF_TYPE__WCM_DJANGO_INTERFACE;
	msg->msg_type = MSG_TYPES__DJANGO_UNREG_AP_REQ;
	msg->data_len = unreg_msg_len;
	memcpy(msg->data, unreg_msg_data, unreg_msg_len);
	comm_len = sizeof(common_msg_t) + unreg_msg_len;
	printf("\nmsg->data[0]:%d\n", msg->data[0]);
#endif
#if 0//Unregister AP
	msg.interface = COMMON_MSG__IF_TYPE__WCM_DJANGO_INTERFACE;
	msg.msg_type = MSG_TYPES_DJANGO__UNREG_AP;
	msg.has_serial_bytes = 1;
	msg.serial_bytes.len = unreg_msg_len;
	msg.serial_bytes.data = unreg_msg_data;
#endif
#if 0//Register AP
	msg = malloc(sizeof(common_msg_t) + reg_msg_len);
	if(msg == NULL) {
		perror("malloc");
		goto END;
	}
	msg->interface = IF_TYPE__WCM_CLI_INTERFACE;
	msg->msg_type = MSG_TYPES__CLI_REG_AP_REQ;
	msg->data_len = reg_msg_len;
	memcpy(msg->data, reg_msg_data, reg_msg_len);
	comm_len = sizeof(common_msg_t) + reg_msg_len;
	printf("\nmsg->data[0]:%d\n", msg->data[0]);
#endif

#if 0//Register AP
	msg.interface = COMMON_MSG__IF_TYPE__WCM_DJANGO_INTERFACE;
	msg.msg_type = MSG_TYPES_DJANGO__REG_AP;
	msg.has_serial_bytes = 1;
	msg.serial_bytes.len = reg_msg_len;
	msg.serial_bytes.data = reg_msg_data;
#endif
#if 0//Create GROUP
	msg = malloc(sizeof(common_msg_t) + group_msg_len);
	if(msg == NULL) {
		perror("malloc");
		goto END;
	}
	msg->interface = IF_TYPE__WCM_SNMP_INTERFACE;
	msg->msg_type = MSG_TYPES__SNMP_DELETE_GROUP_REQ;
	msg->data_len = group_msg_len;
	memcpy(msg->data, group_msg_data, group_msg_len);
	comm_len = sizeof(common_msg_t) + group_msg_len;
	printf("\nmsg->data[0]:%d\n", msg->data[0]);

#endif

#if 0//Create GROUP
	msg.interface = COMMON_MSG__IF_TYPE__WCM_SNMP_INTERFACE;
	msg.msg_type = MSG_TYPES_SNMP__CREATE_GROUP;
	msg.has_serial_bytes = 1;
	msg.serial_bytes.len = group_msg_len;
	msg.serial_bytes.data = group_msg_data;
#endif
#if 0//Create VAP
	msg = malloc(sizeof(common_msg_t) + vap_msg_len);
	if(msg == NULL) {
		perror("malloc");
		goto END;
	}
	msg->interface = IF_TYPE__WCM_SNMP_INTERFACE;
	msg->msg_type = MSG_TYPES__SNMP_CREATE_VAP_REQ;
	msg->data_len = vap_msg_len;
	memcpy(msg->data, vap_msg_data, vap_msg_len);
	comm_len = sizeof(common_msg_t) + vap_msg_len;
	printf("\nmsg->data[0]:%d\n", msg->data[0]);
#endif
#if 0//Delete VAP
	msg = malloc(sizeof(common_msg_t) + vap_msg_len);
	if(msg == NULL) {
		perror("malloc");
		goto END;
	}
	msg->interface = IF_TYPE__WCM_SNMP_INTERFACE;
	msg->msg_type = MSG_TYPES__SNMP_DELETE_VAP_REQ;
	msg->data_len = vap_msg_len;
	memcpy(msg->data, vap_msg_data, vap_msg_len);
	comm_len = sizeof(common_msg_t) + vap_msg_len;
	printf("\nmsg->data[0]:%d\n", msg->data[0]);
#endif

#if 0//Create GROUP
	msg.interface = COMMON_MSG__IF_TYPE__WCM_CLI_INTERFACE;
	msg.msg_type = MSG_TYPES_CLI__CREATE_GROUP;
	msg.has_serial_bytes = 1;
	msg.serial_bytes.len = group_msg_len;
	msg.serial_bytes.data = group_msg_data;
#endif
#if 0//Create VAP
	msg.interface = COMMON_MSG__IF_TYPE__WCM_CLI_INTERFACE;
	msg.msg_type = MSG_TYPES_CLI__CREATE_VAP;
	msg.has_serial_bytes = 1;
	msg.serial_bytes.len = vap_msg_len;
	msg.serial_bytes.data = vap_msg_data;
#endif
#if 0//Update VAP
	msg = malloc(sizeof(common_msg_t) + vap_msg_len);
	if(msg == NULL) {
		perror("malloc");
		goto END;
	}
	msg->interface = IF_TYPE__WCM_CLI_INTERFACE;
	msg->msg_type = MSG_TYPES__CLI_UPDATE_VAP_REQ;
	msg->data_len = vap_msg_len;
	memcpy(msg->data, vap_msg_data, vap_msg_len);
	comm_len = sizeof(common_msg_t) + vap_msg_len;
	printf("\nmsg->data[0]:%d\n", msg->data[0]);

#endif

#if 0//Update VAP
	msg.interface = COMMON_MSG__IF_TYPE__WCM_CLI_INTERFACE;
	msg.msg_type = MSG_TYPES_CLI__UPDATE_VAP;
	msg.has_serial_bytes = 1;
	msg.serial_bytes.len = vap_msg_len;
	msg.serial_bytes.data = vap_msg_data;
#endif
#if 0//Update Group
	msg = malloc(sizeof(common_msg_t) + group_msg_len);
	if(msg == NULL) {
		perror("malloc");
		goto END;
	}
	msg->interface = IF_TYPE__WCM_CLI_INTERFACE;
	msg->msg_type = MSG_TYPES__CLI_UPDATE_GROUP_REQ;
	msg->data_len = group_msg_len;
	memcpy(msg->data, group_msg_data, group_msg_len);
	comm_len = sizeof(common_msg_t) + group_msg_len;
	printf("\nmsg->data[0]:%d\n", msg->data[0]);
#endif
#if 0//add GROUP VAP
	msg = malloc(sizeof(common_msg_t) + group_ssid_len);
	if(msg == NULL) {
		perror("malloc");
		goto END;
	}
	msg->interface = IF_TYPE__WCM_SNMP_INTERFACE;
	msg->msg_type = MSG_TYPES__SNMP_GROUP_VAP_DELETE_REQ;
	msg->data_len = group_ssid_len;
	memcpy(msg->data, group_ssid_data, group_ssid_len);
	comm_len = sizeof(common_msg_t) + group_ssid_len;
	printf("\nmsg->data[0]:%d\n", msg->data[0]);
#endif


#if 0//Update Group
	msg.interface = COMMON_MSG__IF_TYPE__WCM_CLI_INTERFACE;
	msg.msg_type = MSG_TYPES_CLI__UPDATE_GROUP;
	msg.has_serial_bytes = 1;
	msg.serial_bytes.len = group_msg_len;
	msg.serial_bytes.data = group_msg_data;
#endif

	/*Fill common msg, send  and recv data*/
#if 0
	msg.random_id = 5;
	comm_len = common_msg__get_packed_size(&msg); 
	msg.data_len = comm_len;/*packet length +2 if comm_len is greater than 65535*/
	printf("\ncommon message len : %d\n", comm_len);
	comm_msg_data = malloc(comm_len);
	if(comm_msg_data == NULL)
	{
		printf("\nmalloc error\n");
		goto END;
	}
#ifdef FILEWRITE
    fptr = fopen("dump.txt", "a");
    if(fptr == NULL) {
        printf("\nerror: %s\n", strerror(errno));
		return -1;
    }
	if((fwrite(&msg, sizeof(unsigned char), comm_len, fptr)) != comm_len) {
		printf("\nerror: %s\n", strerror(errno));
	} else {
		printf("\nFWR\n");
	}
	fclose(fptr);
#endif

	len = common_msg__pack(&msg, comm_msg_data);
	if(len <=0)
	{
		printf("\npacking fail\n");
		goto END;
	}
#endif
	msg->sof = MSG_SOF;
	for(i=0; i<MSG_COUNT; i++) {
		pthread_mutex_lock(&lock);
		ret = send(sock_fd, msg, comm_len, 0);

		if(ret > 0)
		{
			printf("\nsent:%d\n", ret);
		}
		pthread_mutex_unlock(&lock);
		pthread_mutex_lock(&lock);
		if(i%4 == 0) {
			memset(recv_buf, 0, sizeof(recv_buf));
			ret = recv(sock_fd, recv_buf, sizeof(recv_buf), 0);
			printf("\nrecv len: %d\n", ret);
#if 0
			if(ret > 0) {
				printf("\nrecv len: %d\n", ret);
				comm_msg_proto_recv = common_msg__unpack(NULL, ret, recv_buf);
				if(comm_msg_proto_recv == NULL)
				{
					printf("\nUnpack common msg failed\n");
					pthread_mutex_unlock(&lock);
					continue;
				}
				if(comm_msg_proto_recv->has_response == 1) {
					printf("\ncommon msg recv: response:%d\n", comm_msg_proto_recv->response);
				}
				if(comm_msg_proto_recv->has_serial_bytes == 1) {
					printf("\nrecv len: %ld\n", comm_msg_proto_recv->serial_bytes.len );
				}
			}
#endif
		}
		pthread_mutex_unlock(&lock);
	}
	while(1);

END:
	/*free(ap_msg.ap_mac.data);*/
	/*free(unreg_msg.mac.data);*/
	/*free(vap_msg_data);*/
	/*free(group_msg_data);*/
	/*free(reg_msg_data);*/
	/*free(unreg_msg_data);*/
#if 0
	if(comm_msg_proto_recv != NULL) {
		common_msg__free_unpacked(comm_msg_proto_recv, NULL);
	}
#endif
	return NULL;
}
int main()
{
	struct sockaddr_in srcAddr;
	int sock_fd, ret, i;
	pthread_t tid[THREAD_COUNT];
	printf("2");	
	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (-1 == sock_fd) {
		printf ("socket creation error\r\n");
		/*return -1;*/
	}

	printf ("connecting to host: %s on 7000\r\n", MVM_IP);
	srcAddr.sin_port = htons(7000);
	srcAddr.sin_addr.s_addr = inet_addr(MVM_IP);
	srcAddr.sin_family = AF_INET;

	if (-1 == connect (sock_fd, (struct sockaddr *)&srcAddr, sizeof(srcAddr))) {
		printf ("error connecting to %s:7000\r\n", MVM_IP);
		close (sock_fd);
		/*return -1;*/
	} else {
		printf("\r\nConnected\r\n");
		if (pthread_mutex_init(&lock, NULL) != 0)
		{
			printf("\n mutex init has failed\n");
			return 1;
		}
		for(i = 0; i<THREAD_COUNT; i++) {
			ret = pthread_create(&(tid[i]), NULL, &trythis, (void *)&sock_fd);
			if (ret != 0)
				printf("\nThread can't be created : [%s]", strerror(ret));
		}
		while(1);
	}
	pthread_mutex_destroy(&lock);
	return 0;
}

