/********************************************************************************
 *
 *  @file        :  wcm.c
 *  @brief       :  WLC Config Manager serves configuration for AP's
 *  @author      :  VVDN Technologies Pvt. Ltd.
 *  Copyright    :  (c) 2017-2018 , VVDN Technologies Pvt. Ltd.
 Permission is hereby granted to everyone in VVDN Technologies
 to use the Software without restriction,including without
 limitation the rights to use, copy, modify, merge, publish,
 distribute, distribute with modifications.
 *
 ********************************************************************************/
#include <stdarg.h>
#include "wcm.h"
#include "cass_prototypes.h"
#include "wlc_get_interface_ip.h"
#include "wlc_cass_error_strings.h"
#include "wlc_proc_info.h"
int cmt1[DMI_SOCKETS_COUNT];
int cmt2[DMI_SOCKETS_COUNT];
int cmt3[DMI_SOCKETS_COUNT];
int cmt4[DMI_SOCKETS_COUNT];
int dmi_sockets1[DMI_SOCKETS_COUNT];
int dmi_sockets2[DMI_SOCKETS_COUNT];
int dmi_sockets[DMI_SOCKETS_COUNT];
int dmi_server_sock = 0;
int max_sd=0;
int ac_server_sock = 0;
int max_sd_ac=0;
char mvm_ip[IP_STR_LENGTH];
char mvm_ipy[IP_STR_LENGTH];
char mvm_ipz[IP_STR_LENGTH];
unsigned char cvm_ip[IPV4_LEN];
cvm_info_t cvm_arr[AC_SOCKETS_COUNT];
struct wlcl_ctx_mutex wlcl_ctx_head;
//test4
int select_call_read_check(int sock)
{
	int ret;
	fd_set readset;
	struct timeval t;

	t.tv_usec=0;
	t.tv_sec= SOCKET_TIMEOUT_SECONDS;/*Check FD_ISSET for Timeout seconds*/
	FD_ZERO(&readset);/*Clear readset*/
	FD_SET(sock, &readset);/*Set socket descriptor if any incoming data*/
	ret = select(sock+1, &readset, NULL, NULL, &t);/*FD_SETSIZE or (higher descriptor +1)*/
	if(ret > 0) { /*something to read*/
		if(FD_ISSET(sock, &readset)) {
			return WCM_SUCCESS;
		}
	}
	return WCM_FAIL;
}

int sock_send(int sock, unsigned char *buf, unsigned int len)
{
	unsigned char *ptr;
	int ret;
	unsigned int send_len = 0;
	
	ptr = buf;
	while(1) {
		if((len-send_len) > SEND_BUFFER_SIZE) {
			ret = send(sock, ptr, SEND_BUFFER_SIZE, 0);/*try to send max MTU*/
		} else {
			ret = send(sock, ptr, (len-send_len), 0);
		}
		if(ret > 0) {
			ptr += ret;
			send_len += ret;
			if(ret < SEND_BUFFER_SIZE) {/*If less bytes sent it should be last send*/
				break;
			}
		}
		else if(ret <= 0) {/*if ret < = 0 , recv failed*/
			app_log (LOG_DEBUG, "%s send %s", __func__, strerror(errno));
			break;
		}
	}

	return send_len;
}
//test_sanjay
int sock_recv(int sock, unsigned char *buf, unsigned int max_len)
{
	unsigned char *ptr;
	int ret;
	unsigned int recv_len = 0;
	
	ptr = buf;
	while(1) {
		if((max_len - recv_len) >= RECV_BUFFER_SIZE) {/*Dont exceed max length as buffer size overflows*/
			ret = recv(sock, ptr, RECV_BUFFER_SIZE, 0);/*try to receive max MTU*/
		} else {
			ret = recv(sock, ptr, (max_len-recv_len), 0);
		}
		if(ret > 0) {
			ptr += ret;/*increase buffer pointer*/
			recv_len += ret;/*Increase total recv length*/
			if(ret < RECV_BUFFER_SIZE) {/*If less bytes received it should be last recv so break and come out*/
				break;
			}
		} else if(ret <= 0) {/*if ret < = 0 , recv failed*/
			app_log (LOG_DEBUG, "%s recv %s", __func__, strerror(errno));
			break;
		}
	}
	return recv_len;/*return received length*/
}

int broadcast_to_ac(gcp_vap_info_t *update, int msg_type)
{
	int ret, socket_index, ret_val = WCM_SUCCESS;
	common_msg_t *msg=NULL;

	msg = (common_msg_t *)malloc(sizeof(common_msg_t)+sizeof(gcp_vap_info_t));
	if(msg->data == NULL) {
		app_log (LOG_ERR, "malloc common_msg_t *msg: %s", strerror(errno));
		ret_val = WCM_FAIL;
		goto END;
	}

	memset(msg, 0, sizeof(common_msg_t));
	msg->interface = IF_TYPE__WCM_AC_INTERFACE;
	msg->msg_type = msg_type;
	msg->data_len = sizeof(gcp_vap_info_t);
	memcpy(msg->data, update, sizeof(gcp_vap_info_t));

	for(socket_index=0; socket_index<AC_SOCKETS_COUNT; socket_index++) {
		if(cvm_arr[socket_index].ac_socket != -1) {
			ret = send_proto_common_msg(cvm_arr[socket_index].ac_socket, msg, sizeof(common_msg_t)+sizeof(gcp_vap_info_t));
			if(ret == WCM_FAIL) {
				ret_val = WCM_FAIL;
				goto FREE;
			}
		}
	}
FREE:
	if(msg != NULL)
		free(msg);
END:
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int update_unreg_status_to_ac(ap_metadata_t *ap_meta, wtp_basic_details_t *wtp_info)
{
	int cvm_sock, ret, ret_val = 0;

	app_log (LOG_INFO, "in %s cvm_ip: %d:%d:%d:%d", __func__, ap_meta->cvm_ip[0], ap_meta->cvm_ip[1], ap_meta->cvm_ip[2], ap_meta->cvm_ip[3]);
	ret = get_sock_from_cvmip(ap_meta->cvm_ip, &cvm_sock);
	if(ret == WCM_SUCCESS && cvm_sock > -1) {
		ret = send_unreg_wtp_to_ac(wtp_info, cvm_sock);
		if(ret == WCM_SUCCESS) {
			ret_val = WCM_SUCCESS;
			goto END;
		} else {
			app_log (LOG_WARNING, "Sending Unreg status to AC failed");
		}
	} else {
		app_log (LOG_DEBUG, "cvmip-cvm_sock is not in WCM table");
	}
	/*Store in Unreg List if sent failed or cvm info not available*/
	if(ap_meta->wtp_id != INVALID_WTP_ID) {
		app_log (LOG_INFO, "Write ap meta to unregister table");
		ret = cass_in_update_unreg_ap_meta(wtp_info->macaddr, ap_meta);
		if (ret < 0) {
			wlc_cass_print_error(ret);
			ret_val = WCM_FAIL;
			goto END;
		}
	}
END:
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int send_pending_unreg_msgs_to_cvm(unsigned char *cvm_ip, int sock)
{
	int iter, ret, ret_val = 0;
	unsigned int count;
	ap_info_t **ap;
	wtp_basic_details_t wtp_info;

	app_log (LOG_INFO, "cvm_ip: %d:%d:%d:%d", cvm_ip[0], cvm_ip[1], cvm_ip[2], cvm_ip[3]);
	ret = cass_out_unreg_mac_count_in_cvm_ip(cvm_ip, &count);
	if((ret == WCM_SUCCESS)&&(count > 0)) {
		/*Allocate memory for unreg ap count*/
		app_log (LOG_DEBUG, "unreg mac count: %d", count);
		ap  = (ap_info_t **) malloc(count * sizeof(ap_info_t *));
		if (ap == NULL) {
			ret_val = WCM_FAIL;
			app_log (LOG_ERR, "malloc ap: %s", strerror(errno));
			goto END;
		}
		for (iter = 0;iter < count; iter++) {
			ap[iter] = malloc(sizeof(ap_info_t));
			if (ap[iter] == NULL) {
				for (;iter >= 0; iter--) {
					free(ap[iter]);
				}
				free(ap);
				ret_val = WCM_FAIL;
				app_log (LOG_ERR, "malloc ap[iter]: %s", strerror(errno));
				goto END;
			}
		}
		/*get Pending Unreg MACs for the CVM*/
		if(cass_out_unreg_mac_cvm_ip(cvm_ip, count, ap) == SUCCESS) {
			app_log (LOG_INFO, "Sending Pending unreg AP to AC");
			for(iter=0; iter<count; iter++) {
				memset(&wtp_info, 0, sizeof(wtp_basic_details_t));
				memcpy(wtp_info.macaddr, ap[iter]->ap_mac, sizeof(wtp_info.macaddr));
				wtp_info.wtp_id = ap[iter]->ap_meta.wtp_id;
				/*Send Unreg message to AC*/
				ret = send_unreg_wtp_to_ac(&wtp_info, sock);
				if(ret == WCM_FAIL) {
					app_log (LOG_DEBUG, "Error: Sending unreg AP info to AC, total:%d, fail:%d", count, count-(iter+1));
					ret_val = WCM_FAIL;
					break;
				} else {/*If send success, set unreg table with default values*/
					memset(ap[iter]->ap_meta.cvm_ip, 0, IPV4_LEN);
					ap[iter]->ap_meta.wtp_id = INVALID_WTP_ID;
					ret = cass_in_update_unreg_ap_meta(wtp_info.macaddr, &ap[iter]->ap_meta);
					if (ret < 0) {
						wlc_cass_print_error(ret);
					}
				}
			}
		} else {
			wlc_cass_print_error(ret);
			ret_val = WCM_FAIL;
			goto FREE;
		}
	} else {
		wlc_cass_print_error(ret);
		goto END;
	}

FREE:
	for (iter = 0;iter < count; iter++) {
		free(ap[iter]);
	}
	free(ap);

END:
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val; 
}

int get_cvmip_from_sock(int sock, unsigned char *ip)
{
	int socket_index;

	for(socket_index=0; socket_index<AC_SOCKETS_COUNT; socket_index++) {
		if(sock == cvm_arr[socket_index].ac_socket) {
			memcpy(ip, cvm_arr[socket_index].ip, sizeof(cvm_arr[socket_index].ip));
			return WCM_SUCCESS;
		}
	}
	return WCM_FAIL;
}

int get_sock_from_cvmip(unsigned char *ip, int *sock)
{
	int socket_index;

	for(socket_index=0; socket_index<AC_SOCKETS_COUNT; socket_index++) {
		if((memcmp(cvm_arr[socket_index].ip, ip, IPV4_LEN))==0) {
			*sock = cvm_arr[socket_index].ac_socket;
			return WCM_SUCCESS;
		}
	}
	return WCM_FAIL;
}

int delete_mac_from_bsl(unsigned char *mac)
{
	int ret;
	struct list_head *loop_node = NULL;
	struct bootstrap* current = NULL;
	/*Mutex lock*/
	if(wlcl_block_lock_mutex(&wlcl_ctx_head) != WCM_SUCCESS) {
		app_log (LOG_WARNING, "acquire lock wlcl_ctx_head failed");
		return WCM_FAIL;
	}

	list_for_each(loop_node, &(wlcl_ctx_head.head)) {
		current = list_entry(loop_node, struct bootstrap, head_ptr);
		if(NULL != current) {
			ret = memcmp(current->mac, mac, MAC_LEN);
			if(ret == 0) {
				app_log (LOG_INFO, "mac found");
				wlcl_del_node(current);/*Deleted node*/
				free(current);
				/*Mutex unlock*/
				wlcl_unlock_mutex(&wlcl_ctx_head);
				return WCM_SUCCESS;/*Set success status to send bootstrap success*/
			}
		}
	}
	app_log (LOG_DEBUG, "Mac %02x:%02x:%02x:%02x:%02x:%02x not found in BSL", \
			mac[0] ,mac[1], mac[2], \
			mac[3], mac[4], mac[5]);
	/*Mutex unlock*/
	wlcl_unlock_mutex(&wlcl_ctx_head);

	return WCM_FAIL;
}

int check_bootstrap(unsigned char *macaddr)
{
	app_log (LOG_DEBUG, "%s Mac %02x:%02x:%02x:%02x:%02x:%02x", __func__,\
			macaddr[0] ,macaddr[1], macaddr[2], \
			macaddr[3], macaddr[4], macaddr[5]);

	if(delete_mac_from_bsl(macaddr) == WCM_SUCCESS) {
		app_log (LOG_DEBUG, "bootstrapped");
		return WCM_SUCCESS;
	} else {
		app_log (LOG_DEBUG, "not bootstrapped");
		return WCM_FAIL;
	}
}

int send_proto_common_msg(int sock, common_msg_t *msg, int comm_len)
{
	int ret, ret_val=0;

	msg->sof = MSG_SOF;
	ret = sock_send(sock, (unsigned char*)msg, comm_len);
	if(ret != comm_len) {
		app_log (LOG_DEBUG, "sent %d bytes of %d bytes", ret, comm_len);
		ret_val = WCM_FAIL;
	}
	return ret_val;
} 

int process_img_upgrade_request(ImageUpgrade *command_img_upgrade)
{
	char upg_handler[256];
	int ret = 0;

	if (access (WLC_UPGRADE_CMD, X_OK) < 0) {
		app_log (LOG_CRIT, WLC_UPGRADE_CMD": %s\n", strerror (errno));
		return WCM_FAIL;
	} else if((command_img_upgrade->filepath == NULL) || \
			(command_img_upgrade->filename == NULL)) {
		app_log (LOG_ERR, "img upg params - path: %p file: %p\n", 
				command_img_upgrade->filepath, command_img_upgrade->filename);
		return WCM_FAIL;
	}

	sprintf (upg_handler, "%s%s", \
			command_img_upgrade->filepath, command_img_upgrade->filename);
	app_log (LOG_DEBUG, "upgrade file: %s\n", upg_handler);

	if (access (upg_handler, R_OK) < 0) {
		app_log (LOG_ERR, "image %s: %s\n", upg_handler, strerror (errno));
		return WCM_FAIL;
	}
	sprintf (upg_handler, WLC_UPGRADE_CMD"%c%s%s", \
			' ',command_img_upgrade->filepath, command_img_upgrade->filename);

	if ((ret = system (upg_handler)) == -1) {
		app_log (LOG_ERR, "upg: %s error: %s\n", upg_handler, strerror (errno));
		return WCM_FAIL;
	}

	return WCM_SUCCESS;
}

int process_set_ap_status_request(wtp_basic_details_t *wtp)
{
	int ret;
	reg_ap_info_t reg_ap;			

	ret = cass_out_get_registered_ap_info(wtp->macaddr, &reg_ap);
	if(ret == 0) {/*Set status if AP in registered list*/
		app_log (LOG_DEBUG, "AP status:%d", wtp->wtp_status);
		ret = cass_in_set_reg_ap_status(wtp->wtp_status, wtp->macaddr);
		if(ret == WCM_FAIL) {
			app_log (LOG_WARNING, "error: AP status set failed");
			return WCM_FAIL;
		}
	}
	return WCM_SUCCESS;
}

int process_ac_config_request(ac_basic_config_t *ac_config)
{
	memcpy(ac_config->acName, "WLC_AC1", strlen("WLC_AC1"));
	ac_config->acHWver = 893084;
	ac_config->acSWver = 3574854;
	ac_config->type = 1;
	ac_config->maxWtps = 100;
	ac_config->mtu_size = 1420;
	return WCM_SUCCESS;
}

int process_check_bootstrap_request(wtp_basic_details_t *wtp, int sock)
{
	int ret, ret_val = 0;
	ap_metadata_t ap_meta;
	memset(&ap_meta, 0, sizeof(ap_metadata_t));

	app_log (LOG_INFO, "MAC: %02x:%02x:%02x:%02x:%02x:%02x", wtp->macaddr[0], wtp->macaddr[1], wtp->macaddr[2], wtp->macaddr[3], wtp->macaddr[4], wtp->macaddr[5]);
	ret = check_bootstrap(wtp->macaddr);
	if(ret == WCM_SUCCESS) {/*Store wtpid & cvm IP in DB if bootstrap OK*/
		ap_meta.wtp_id = wtp->wtp_id;
		ret = get_cvmip_from_sock(sock, ap_meta.cvm_ip);
		if(ret == WCM_SUCCESS) {
			ret = cass_in_update_reg_ap_meta(wtp->macaddr, &ap_meta);
			if (ret < 0) {
				wlc_cass_print_error(ret);
				ret_val = WCM_FAIL;
			}
		} else {
			app_log (LOG_WARNING, "Error: CVM IP - sock is not present in WCM table");
			ret_val = WCM_FAIL;
		}
	} else {
		ret_val = WCM_FAIL;
	}
	return ret_val;
}

int process_vap_delete_request(char * vapid)
{
	int ret, iter;
	unsigned int count = 0;
	gcp_vap_info_t **gcp_vap = NULL;
	if(vapid == NULL) {
		app_log (LOG_WARNING, "Invalid vapid");
		return WCM_FAIL;
	}
	ret = cass_check_vap_presence(vapid);
	if(ret == WCM_SUCCESS) {
		ret = cass_delete_vap(vapid);
		if (ret < 0) {
			wlc_cass_print_error(ret);
			return WCM_FAIL;
		} else {
			app_log (LOG_INFO, "vap :%s deleted", vapid);
			ret = cass_out_get_gcp_count_of_vap(&count, vapid);
			if(ret == WCM_SUCCESS && count > 0) {
				app_log (LOG_INFO, "delete vap :%s from %d groups", vapid, count);
				gcp_vap = (gcp_vap_info_t **)malloc(count * sizeof(gcp_vap_info_t*));
				if(gcp_vap == NULL) {
					app_log (LOG_ERR, "malloc gcp_vap_info_t *gcp_vap: %s", strerror(errno));
					goto END;/*Return success as we need to broadcast vap delete status to AC, TODO: handle delete vap from groups*/
				}
				for(iter=0; iter<count; iter++) {
					gcp_vap[iter] = (gcp_vap_info_t *)malloc(sizeof(gcp_vap_info_t));
					if(gcp_vap[iter] == NULL) {
						app_log (LOG_ERR, "malloc gcp_vap_info_t *gcp_vap: %s", strerror(errno));
						goto END;/*Return success as we need to broadcast vap delete status to AC, TODO: handle delete vap from groups*/
					}
				}
				ret = cass_out_get_gcp_list_of_vap(count, gcp_vap, vapid);
				if(ret != WCM_SUCCESS) {
					app_log (LOG_ERR, "failed cass_out_get_gcp_list_of_vap: vapid %s", vapid);
					goto END;/*Return success as we need to broadcast vap delete status to AC, TODO: handle delete vap from groups*/
				}
				for(iter=0; iter<count; iter++) {
					app_log (LOG_ERR, "cass_in_delete_gcp_vap: vapid %s groupid:%s", gcp_vap[iter]->vapID, gcp_vap[iter]->groupID);
					ret = cass_in_delete_gcp_vap(gcp_vap[iter]);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_ERR, "failed cass_in_delete_gcp_vap: vapid %s groupid:%s", gcp_vap[iter]->vapID, gcp_vap[iter]->groupID);
						goto END;/*Return success as we need to broadcast vap delete status to AC, TODO: handle delete vap from groups*/
					}
				}
			} else {
				wlc_cass_print_error(ret);
			}
		}
	} else {
		wlc_cass_print_error(ret);
		return WCM_FAIL;
	}
END:
	for (iter = 0;iter < count; iter++) {
		if(gcp_vap[iter] != NULL)
		free(gcp_vap[iter]);
	}
	if(gcp_vap != NULL)
		free(gcp_vap);
	return WCM_SUCCESS;
}

int process_group_delete_request(char * groupid)
{
	int ret;
	if(groupid == NULL) {
		app_log (LOG_WARNING, "Invalid groupid");
		return WCM_FAIL;
	}
	ret = cass_check_gcp_presence(groupid);
	if(ret == WCM_SUCCESS) {
		ret = cass_delete_gcp(groupid);
		if (ret < 0) {
			wlc_cass_print_error(ret);
			return WCM_FAIL;
		} else {
			app_log (LOG_INFO, "group :%s deleted", groupid);
			ret = cass_in_delete_gcp_gcp_vap(groupid);
			if(ret != WCM_SUCCESS) {
				app_log (LOG_CRIT, "group :%s delete from gcp_vap table failed", groupid);/*TODO: Handle this case*/
			}
		}
	} else {
		wlc_cass_print_error(ret);
		return WCM_FAIL;
	}
	return WCM_SUCCESS;
}

int process_gcp_vap_delete_request(gcp_vap_info_t * group_ssid)
{
	int ret;

	ret = cass_in_delete_gcp_vap(group_ssid);
	if (ret < 0) {
		wlc_cass_print_error(ret);
		return WCM_FAIL;
	} else {
		app_log (LOG_INFO, "In group:%s vap: %s deleted", group_ssid->groupID, group_ssid->vapID);
	}
	return WCM_SUCCESS;
}

int process_gcp_vap_map_request(GroupSsidConfig *group_ssid)
{
	int ret, arr, ret_val = WCM_SUCCESS;
	unsigned int count = 1;/*TODO: count, vapid[count], radio_type[count] should be added in GroupSsidConfig for multiple vaps*/
	update_meta_t meta;
	memset(&meta, 0, sizeof(update_meta_t));
	gcp_vap_info_t *gcp_vap = NULL;
	
	gcp_vap = malloc(count * sizeof(gcp_vap_info_t));
	if(gcp_vap == NULL) {
		app_log (LOG_ERR, "malloc count: %d gcp_vap_info_t *update: %s", count, strerror(errno));
		ret_val = WCM_FAIL;
		goto FREE;
	}
	memset(gcp_vap, 0, count * sizeof(gcp_vap_info_t));
	for(arr=0; arr<count; arr++) {
		if(group_ssid->groupid != NULL) {
			memcpy((gcp_vap+arr)->groupID, group_ssid->groupid, sizeof((gcp_vap+arr)->groupID));
		}
		if(group_ssid->vapid != NULL) {
			memcpy((gcp_vap+arr)->vapID, group_ssid->vapid, sizeof((gcp_vap+arr)->vapID));
		}
		if(group_ssid->has_radiotype == 1) {
			(gcp_vap+arr)->radio_type = (signed char)group_ssid->radiotype;
		}
	}
	if(count == 1) {/*TODO: possible case to be update*/
		ret = cass_out_get_gcp_vap_info(gcp_vap);
		if(ret == 0) {/*gcp vap already present*/
			meta.is_update = 1;/*set update flag*/
			if(group_ssid->has_radiotype == 1) {
				(gcp_vap)->radio_type = (signed char)group_ssid->radiotype;/*Update radio type*/
			}
		} else {
			wlc_cass_print_error(ret);
		}
	}
	printf("\nradio type: %d %d\n", gcp_vap->radio_type, group_ssid->radiotype);
	ret = cass_in_create_gcp_vap(count, &gcp_vap, &meta);
	if (ret < 0) {
		wlc_cass_print_error(ret);
		ret_val = WCM_FAIL;
		goto FREE;
	} else {
		if(meta.is_update == 1) {
			app_log (LOG_INFO, "Group-SSID gcp id: %s vap id: %s updated", group_ssid->groupid, group_ssid->vapid);
		} else {
			app_log (LOG_INFO, "Group-SSID gcp id: %s vap id: %s mapped", group_ssid->groupid, group_ssid->vapid);
		}
		for(arr=0; arr<count; arr++) {
			ret = broadcast_to_ac(gcp_vap+arr, MSG_TYPES__AP_GROUP_VAP_ADDITION_REQ);
			if(ret != WCM_SUCCESS) {
				app_log (LOG_WARNING, "broadcast update to AC failed");
			}
		}
	}

FREE:
	if(gcp_vap != NULL) {
		free(gcp_vap);
	}
	return ret_val;
}

int process_set_wlc_config_request(WlcSystem *wlc_config)
{
	int ret;
	wlc_info_t wlc_system_config;
	unsigned char mask = 0;

	memset(&wlc_system_config, 0, sizeof(wlc_info_t));
	if(wlc_config->wlc_sysname != NULL) {
		mask |= MASK_WLC_SYSNAME;
		memcpy(wlc_system_config.wlc_name, wlc_config->wlc_sysname, sizeof(wlc_system_config.wlc_name));
	}
	if(wlc_config->wlc_location != NULL) {
		mask |= MASK_WLC_LOCATION;
		memcpy(wlc_system_config.wlc_location, wlc_config->wlc_location, sizeof(wlc_system_config.wlc_location));
	}
	if(wlc_config->wlc_time_zone != NULL) {
		mask |= MASK_WLC_TIMEZONE;
		memcpy(wlc_system_config.wlc_timezone, wlc_config->wlc_time_zone, sizeof(wlc_system_config.wlc_timezone));
	}

	ret = cass_in_set_wlc_info(&wlc_system_config, mask);
	if (ret < 0) {
		wlc_cass_print_error(ret);
		return WCM_FAIL;
	} else if(ret == 0) {
		app_log (LOG_INFO, "WLC info set");
	}
	return WCM_SUCCESS;
}

int process_update_gcp_request(GroupConfig *group_msg)
{
	int ret;
	gcp_info_t group;

	app_log (LOG_INFO, "groupid = %s groupname = %s", group_msg->groupid, group_msg->groupname); 
	/*copy from proto to group structure*/
	memset(&group, 0, sizeof(gcp_info_t));
	if(group_msg->groupid != NULL) {
		ret = cass_check_gcp_presence(group_msg->groupid);
		if (ret == 0) {
			ret = cass_out_get_gcp_config(group_msg->groupid, &group);
			if (ret < 0) {
				wlc_cass_print_error(ret);
				return WCM_FAIL;
			} else {
				app_log (LOG_DEBUG, "Group config read done");
			}
		} else {
			wlc_cass_print_error(ret);
			return WCM_FAIL;
		}
	} else {
		return WCM_FAIL;
	}
	if(group_msg->groupname != NULL) {
		memcpy(group.groupName, group_msg->groupname, sizeof(group.groupName));
	}
	ret = cass_in_create_new_gcp(&group);
	if (ret < 0) {
		wlc_cass_print_error(ret);
		return WCM_FAIL;
	} else if(ret == 0) {
		app_log (LOG_INFO, "Group Updated");
	}
	return WCM_SUCCESS;
}

int process_create_gcp_request(GroupConfig *group_msg)
{
	int ret;
	gcp_info_t group;

	app_log (LOG_INFO, "groupid = %s groupname = %s", group_msg->groupid, group_msg->groupname);
	/*copy from proto to group structure*/
	memset(&group, 0, sizeof(gcp_info_t));
	if(group_msg->groupid != NULL) {
		memcpy(group.groupID, group_msg->groupid, sizeof(group.groupID));
	}
	if(group_msg->groupname != NULL) {
		memcpy(group.groupName, group_msg->groupname, sizeof(group.groupName));
	}
	group.isRadioEnabled24 = 1;
	group.radioTxPower24 = DEFAULT_24_TX_POWER;
	group.radioChannel24 = DEFAULT_24_CHANNEL;
	group.radioMode24 = DEFAULT_24_RADIO_MODE;
	group.isRadioEnabled5 = 1;
	group.radioTxPower5 = DEFAULT_5_TX_POWER;
	group.radioChannel5 = DEFAULT_5_CHANNEL;
	group.radioMode5 = DEFAULT_5_RADIO_MODE;

	memcpy(group.Time_Zone, "utc+05:30", sizeof(group.Time_Zone));

	ret = cass_check_gcp_presence(group.groupID);
	if (ret != -2) {
		wlc_cass_print_error(ret);
		return WCM_FAIL;
	} else {
		ret = cass_in_create_new_gcp(&group);
		if (ret < 0) {
			wlc_cass_print_error(ret);
			return WCM_FAIL;
		} else if(ret == 0) {
			app_log (LOG_INFO, "Group created");
		}
	}
	return WCM_SUCCESS;
}

int process_update_vap_request(VapConfig *vap_msg)
{
	int ret;
	vap_info_t vap;

	if(vap_msg->has_authtype == 1) {
		app_log (LOG_INFO, "authtype = %d passphrase = %s",vap_msg->authtype, vap_msg->passphrase);
	}
	/*copy from proto to vap structure*/
	memset(&vap, 0, sizeof(vap_info_t));
	if(vap_msg->vapid != NULL) {
		ret = cass_check_vap_presence(vap_msg->vapid);
		if (ret == 0) {
			ret = cass_out_get_vap_config(vap_msg->vapid, &vap);
			if (ret < 0) {
				wlc_cass_print_error(ret);
				return WCM_FAIL;
			} else {
				app_log (LOG_DEBUG, "Vap config read done");
			}
		} else {
			wlc_cass_print_error(ret);
			return WCM_FAIL;
		}
	} else {
		app_log (LOG_NOTICE, "in %s Vap id is null", __func__);
		return WCM_FAIL;
	}

	if( (vap_msg->has_authtype) == 1) {
		vap.AuthType = vap_msg->authtype;
	}
	if(vap_msg->passphrase != NULL) {
		memcpy(vap.passphrase, vap_msg->passphrase, sizeof(vap.passphrase));
	}
	if(vap_msg->ssid != NULL) {
		memcpy(vap.ssid, vap_msg->ssid, sizeof(vap.ssid));
	}
	if( (vap_msg->has_macmode) == 1) {
		vap.MACMode = vap_msg->macmode;
	}
	if( (vap_msg->has_broadcastssid) == 1) {
		vap.broadcastSSID = vap_msg->broadcastssid;
	}
	ret = cass_in_create_new_vap(&vap);
	if (ret < 0) {
		wlc_cass_print_error(ret);
		return WCM_FAIL;
	} else if(ret == 0) {
		app_log (LOG_INFO, "Vap Updated");
	}
	return WCM_SUCCESS;
}

int process_create_vap_request(VapConfig *vap_msg)
{
	int ret;
	vap_info_t vap;

	app_log (LOG_INFO, "vapid = %s ssid = %s",vap_msg->vapid, vap_msg->ssid);
	if(vap_msg->has_authtype == 1) {
		app_log (LOG_INFO, "authtype = %d passphrase = %s",vap_msg->authtype, vap_msg->passphrase);
	}
	/*copy from proto to vap structure*/
	memset(&vap, 0, sizeof(vap_info_t));
	if(vap_msg->vapid != NULL) {
		memcpy(vap.vapID, vap_msg->vapid, sizeof(vap.vapID));
	}
	if( (vap_msg->has_authtype) == 1) {
		vap.AuthType = vap_msg->authtype;
	}
	if(vap_msg->passphrase != NULL) {
		memcpy(vap.passphrase, vap_msg->passphrase, sizeof(vap.passphrase));
	}
	if(vap_msg->ssid != NULL) {
		memcpy(vap.ssid, vap_msg->ssid, sizeof(vap.ssid));
	}
	if( (vap_msg->has_macmode) == 1) {
		vap.MACMode = vap_msg->macmode;
	}
	if( (vap_msg->has_broadcastssid) == 1) {
		vap.broadcastSSID = vap_msg->broadcastssid;
	}

	ret = cass_check_vap_presence(vap.vapID);
	if (ret != -2) {
		wlc_cass_print_error(ret);
		return WCM_FAIL;
	} else {
		ret = cass_in_create_new_vap(&vap);
		if (ret < 0) {
			wlc_cass_print_error(ret);
			return WCM_FAIL;
		} else if(ret == 0) {
			app_log (LOG_INFO, "Vap created");
		}
	}
	return WCM_SUCCESS;
}

int process_ap_reg_request(RegisterAP *command_reg_ap)
{
	int ret;
	reg_ap_info_t reg_ap;
	ap_info_t ap;
	memset(&reg_ap, 0, sizeof(reg_ap_info_t));
	memset(&ap, 0, sizeof(ap_info_t));

	if(command_reg_ap->ap_info->has_ap_mac == 1){
		app_log (LOG_INFO, "MAC: %02x:%02x:%02x:%02x:%02x:%02x", command_reg_ap->ap_info->ap_mac.data[0], command_reg_ap->ap_info->ap_mac.data[1], \
				command_reg_ap->ap_info->ap_mac.data[2], command_reg_ap->ap_info->ap_mac.data[3], command_reg_ap->ap_info->ap_mac.data[4], \
				command_reg_ap->ap_info->ap_mac.data[5]);
	}
	app_log (LOG_INFO, "ap name: %s group name: %s group id: %s", command_reg_ap->ap_info->ap_name, command_reg_ap->groupname, command_reg_ap->groupid);
	/*copy from proto to ap structure*/
	memset(&reg_ap, 0, sizeof(reg_ap_info_t));
	if(command_reg_ap->ap_info->has_ap_mac == 1){
		if(command_reg_ap->ap_info->ap_mac.data != NULL) {
			memcpy(reg_ap.basic_ap_info.ap_mac, command_reg_ap->ap_info->ap_mac.data, sizeof(reg_ap.basic_ap_info.ap_mac));
		}
	}
	if(command_reg_ap->ap_info->ap_name != NULL) {
		memcpy(reg_ap.basic_ap_info.ap_name, command_reg_ap->ap_info->ap_name, sizeof(reg_ap.basic_ap_info.ap_name));
	}
	if(command_reg_ap->ap_info->ap_model != NULL) {
		memcpy(reg_ap.basic_ap_info.ap_model, command_reg_ap->ap_info->ap_model, sizeof(reg_ap.basic_ap_info.ap_model));
	}
	if(command_reg_ap->ap_info->has_ap_ipv4_addr == 1){
		if(command_reg_ap->ap_info->ap_ipv4_addr.data != NULL) {
			memcpy(reg_ap.basic_ap_info.ap_ip, command_reg_ap->ap_info->ap_ipv4_addr.data, sizeof(reg_ap.basic_ap_info.ap_ip));
		}
	}
	if(command_reg_ap->groupname != NULL) {
		memcpy(reg_ap.ap_group, command_reg_ap->groupname, sizeof(reg_ap.ap_group));
	}
	if(command_reg_ap->groupid != NULL) {
		memcpy(reg_ap.ap_group_id, command_reg_ap->groupid, sizeof(reg_ap.ap_group_id));
	}
	ret = cass_out_get_unregistered_ap_info(reg_ap.basic_ap_info.ap_mac, &ap);
	switch(ret) {
		case 0:/*Ap available*/
			ret = cass_delete_ap_from_unreg(reg_ap.basic_ap_info.ap_mac);
			if (ret < 0) {
				wlc_cass_print_error(ret);
				return WCM_FAIL;
			}
		case -2:/*Ap not available*/
			ret = cass_in_register_ap(&reg_ap);
			if (ret < 0) {
				wlc_cass_print_error(ret);
				return WCM_FAIL;
			} else if(ret == 0) {
				app_log (LOG_INFO, "Ap %02x:%02x:%02x:%02x:%02x:%02x registered", \
						reg_ap.basic_ap_info.ap_mac[0], reg_ap.basic_ap_info.ap_mac[1], reg_ap.basic_ap_info.ap_mac[2], \
						reg_ap.basic_ap_info.ap_mac[3], reg_ap.basic_ap_info.ap_mac[4], reg_ap.basic_ap_info.ap_mac[5]);
			}
			break;
		default:
			wlc_cass_print_error(ret);
			return WCM_FAIL;
	}
	return WCM_SUCCESS;
}

int process_ap_unreg_request(UnregisterAP *command_unreg_ap)
{
	int ret;
	ap_info_t ap;
	reg_ap_info_t reg_ap;
	wtp_basic_details_t wtp_info;
	memset(&wtp_info, 0, sizeof(wtp_basic_details_t));
	memset(&ap, 0, sizeof(ap));
	memset(&reg_ap, 0, sizeof(reg_ap));

	if(command_unreg_ap->mac.len == MAC_LEN) {
		app_log (LOG_DEBUG, "in %s MAC: %02x:%02x:%02x:%02x:%02x:%02x", __func__, command_unreg_ap->mac.data[0], command_unreg_ap->mac.data[1], \
				command_unreg_ap->mac.data[2], command_unreg_ap->mac.data[3], command_unreg_ap->mac.data[4], \
				command_unreg_ap->mac.data[5]);
	}
	/*copy from proto to ap structure*/
	if(command_unreg_ap->mac.data != NULL) {
		memcpy(ap.ap_mac, command_unreg_ap->mac.data, sizeof(ap.ap_mac));
	}
	ret = cass_out_get_registered_ap_info(ap.ap_mac, &reg_ap);
	if (ret < 0) {
		wlc_cass_print_error(ret);
		return WCM_FAIL;
	} else if(ret == 0) {
		ret = cass_in_insert_ap_to_unreg(&reg_ap.basic_ap_info);
		if (ret < 0) {
			wlc_cass_print_error(ret);
			return WCM_FAIL;
		} else if(ret == 0) {
			ret = cass_delete_ap_from_reg(reg_ap.basic_ap_info.ap_mac);
			if (ret < 0) {
				wlc_cass_print_error(ret);
				return WCM_FAIL;
			}
			app_log (LOG_INFO, "Ap %02x:%02x:%02x:%02x:%02x:%02x unregistered", \
			reg_ap.basic_ap_info.ap_mac[0], reg_ap.basic_ap_info.ap_mac[1], \
			reg_ap.basic_ap_info.ap_mac[2], reg_ap.basic_ap_info.ap_mac[3], \
			reg_ap.basic_ap_info.ap_mac[4], reg_ap.basic_ap_info.ap_mac[5] );
			/*delete mac from bootstrap list if present*/
			ret = delete_mac_from_bsl(reg_ap.basic_ap_info.ap_mac);
			if(ret == WCM_SUCCESS) {
				app_log (LOG_DEBUG, "Mac deleted from bsl");
			}
		}
		memcpy(wtp_info.macaddr, reg_ap.basic_ap_info.ap_mac, sizeof(reg_ap.basic_ap_info.ap_mac));
		wtp_info.wtp_id = reg_ap.basic_ap_info.ap_meta.wtp_id;
		ret = update_unreg_status_to_ac(&reg_ap.basic_ap_info.ap_meta, &wtp_info);
		if(ret == WCM_FAIL) {
			app_log (LOG_WARNING, "Error: update_unreg_status_to_ac");
		}
	}
	return WCM_SUCCESS;
}

int send_ac_basic_config(int sock, struct ac_basic_config *config)
{
	int ret, ret_val=0;
	common_msg_t *config_msg=NULL;

	config_msg = (common_msg_t*)malloc(sizeof(common_msg_t)+sizeof(struct ac_basic_config));
	if(config_msg == NULL) {
		app_log (LOG_ERR, "malloc config_msg.data: %s", strerror(errno));
		ret_val = WCM_FAIL;
		goto END;
	}
	memset(config_msg, 0, sizeof(common_msg_t)+sizeof(struct ac_basic_config));
	config_msg->interface = IF_TYPE__WCM_AC_INTERFACE;
	config_msg->msg_type = MSG_TYPES__AC_CONFIG_RESP;
	config_msg->data_len = sizeof(struct ac_basic_config);
	memcpy(config_msg->data, config, sizeof(struct ac_basic_config));

	ret = send_proto_common_msg(sock, config_msg, sizeof(common_msg_t)+sizeof(struct ac_basic_config));
	if(ret == WCM_FAIL) {
		ret_val = WCM_FAIL;
		goto FREE;
	}
FREE:
	if(config_msg != NULL)
		free(config_msg);
END:
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int send_unreg_wtp_to_ac(wtp_basic_details_t *wtp_info, int sock)
{
	int ret, ret_val=0;
	common_msg_t *status_msg=NULL;

	status_msg = (common_msg_t *)malloc(sizeof(common_msg_t)+sizeof(wtp_basic_details_t));
	if(status_msg->data == NULL) {
		app_log (LOG_ERR, "malloc config_msg.data: %s", strerror(errno));
		ret_val = WCM_FAIL;
		goto END;
	}
	
	memset(status_msg, 0, sizeof(common_msg_t));
	status_msg->interface = IF_TYPE__WCM_AC_INTERFACE;
	status_msg->msg_type = MSG_TYPES__AP_UNREG_REQ;
	status_msg->data_len = sizeof(wtp_basic_details_t);
	memcpy(status_msg->data, wtp_info, sizeof(wtp_basic_details_t));
	ret = send_proto_common_msg(sock, status_msg, sizeof(common_msg_t)+sizeof(wtp_basic_details_t));
	if(ret == WCM_FAIL) {
		ret_val = WCM_FAIL;
		goto FREE;
	}
FREE:
	if(status_msg != NULL)
		free(status_msg);
END:
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int send_resp_to_django(int status, int sock, MsgTypes msg_type, int random_id)
{
	int ret, ret_val=0;
	common_msg_t status_msg;

	memset(&status_msg, 0, sizeof(common_msg_t));
	status_msg.request_id = random_id;
	status_msg.response = status;
	status_msg.interface = IF_TYPE__WCM_DJANGO_INTERFACE;
	status_msg.msg_type = msg_type;

	ret = send_proto_common_msg(sock, &status_msg, sizeof(common_msg_t));
	if(ret == WCM_FAIL) {
		ret_val = WCM_FAIL;
		goto END;
	}
END:
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int send_resp_to_cli(int status, int sock, MsgTypes msg_type, int random_id)
{
	int ret, ret_val=0;
	common_msg_t status_msg;

	memset(&status_msg, 0, sizeof(common_msg_t));
	status_msg.request_id = random_id;
	status_msg.response = status;
	status_msg.interface = IF_TYPE__WCM_CLI_INTERFACE;
	status_msg.msg_type = msg_type;

	ret = send_proto_common_msg(sock, &status_msg, sizeof(common_msg_t));
	if(ret == WCM_FAIL) {
		ret_val = WCM_FAIL;
		goto END;
	}
END:
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int send_resp_to_snmp(int status, int sock, MsgTypes msg_type, int random_id)
{
	int ret, ret_val=0;
	common_msg_t status_msg;

	memset(&status_msg, 0, sizeof(common_msg_t));
	status_msg.request_id = random_id;
	status_msg.response = status;
	status_msg.interface = IF_TYPE__WCM_SNMP_INTERFACE;
	status_msg.msg_type = msg_type;

	ret = send_proto_common_msg(sock, &status_msg, sizeof(common_msg_t));
	if(ret == WCM_FAIL) {
		ret_val = WCM_FAIL;
		goto END;
	}
END:
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int send_resp_to_ac(int status, int sock, int msg_type, int random_id)
{
	int ret, ret_val=0;
	common_msg_t status_msg;

	memset(&status_msg, 0, sizeof(common_msg_t));
	status_msg.request_id = random_id;
	status_msg.response = status;
	status_msg.interface = IF_TYPE__WCM_AC_INTERFACE;
	status_msg.msg_type = msg_type;

	ret = send_proto_common_msg(sock, &status_msg, sizeof(common_msg_t));
	if(ret == WCM_FAIL) {
		ret_val = WCM_FAIL;
		goto END;
	}
END:
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int send_ap_bootstrap_st_to_ac(int status, int sock, struct wtp_basic_details *wtp)
{
	int ret, ret_val=0;
	common_msg_t *status_msg;

	status_msg = (common_msg_t*)malloc(sizeof(common_msg_t)+sizeof(wtp_basic_details_t));
	if(status_msg == NULL) {
		app_log (LOG_ERR, "malloc config_msg.data: %s", strerror(errno));
		return WCM_FAIL;
	}
	memset(status_msg, 0, sizeof(common_msg_t)+sizeof(wtp_basic_details_t));
	status_msg->response = status;
	status_msg->interface = IF_TYPE__WCM_AC_INTERFACE;
	status_msg->msg_type = MSG_TYPES__AP_BOOTSTRAP_RESP;
	status_msg->data_len = sizeof(struct wtp_basic_details);
	wtp->wtp_status = status;
	memcpy(status_msg->data, wtp, sizeof(struct wtp_basic_details));
	ret = send_proto_common_msg(sock, status_msg, sizeof(common_msg_t)+sizeof(wtp_basic_details_t));
	if(ret == WCM_FAIL) {
		ret_val = WCM_FAIL;
		goto FREE;
	}
FREE:
	if(status_msg != NULL)
		free(status_msg);
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int serve_ac(common_msg_t *comm_msg_data, int sock)
{
	int ret, ret_val=0;
	struct wtp_basic_details wtp;
	struct ac_basic_config ac_config;
	switch(comm_msg_data->msg_type)
	{
		case MSG_TYPES__AP_BOOTSTRAP_REQ:
			{
				app_log (LOG_INFO, "CMD: Check Bootstrap");
				if(comm_msg_data->data != NULL) {
					memset(&wtp, 0, sizeof(struct wtp_basic_details));
					memcpy(&wtp, comm_msg_data->data, sizeof(struct wtp_basic_details));
					ret = process_check_bootstrap_request(&wtp, sock);
					if(ret == WCM_FAIL) {
						ret_val = WCM_FAIL;
						goto END;
					}
				} else {
					app_log (LOG_WARNING, "AC: CHECK_BOOTSTRAP common msg is null");
				}
			}
			break;
		case MSG_TYPES__AC_CONFIG_REQ:
			{
				unsigned char cvm_ip[4];

				memset(&ac_config, 0, sizeof(ac_config));
			
				ret = get_cvmip_from_sock(sock, cvm_ip);
				if(ret == WCM_FAIL) {
					app_log (LOG_ERR, "Error: CMD: AC_CONFIG CVM IP - sock is not present in WCM table");
					return WCM_FAIL;
				}
				app_log (LOG_INFO, "CMD: AC_CONFIG, AC IP: %d.%d.%d.%d", cvm_ip[0], cvm_ip[1], cvm_ip[2], cvm_ip[3]);
				ret = process_ac_config_request(&ac_config);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
					goto END;
				}
			}
			break;
		case MSG_TYPES__AP_STATUS:
			{
				if(comm_msg_data->data != NULL) {
					memset(&wtp, 0, sizeof(struct wtp_basic_details));
					memcpy(&wtp, comm_msg_data->data, sizeof(struct wtp_basic_details));
					app_log (LOG_INFO, "CMD: SET_AP_STATUS, AP Mac: %02x:%02x:%02x:%02x:%02x:%02x\n",\
					wtp.macaddr[0], wtp.macaddr[1], wtp.macaddr[2], \
					wtp.macaddr[3], wtp.macaddr[4], wtp.macaddr[5]);
					ret = process_set_ap_status_request(&wtp);
					if(ret == WCM_FAIL) {
						ret_val = WCM_FAIL;
						goto END;
					}
				} else {
					app_log (LOG_WARNING, "CMD: AP_STATUS common msg is null");
				}
			}
			break;
		default:
			app_log (LOG_WARNING, "Message type %d not supported", comm_msg_data->msg_type);
			break;
	}
END:
	switch(comm_msg_data->msg_type) {
		case MSG_TYPES__AP_BOOTSTRAP_REQ:
			ret = send_ap_bootstrap_st_to_ac(!ret_val, sock, &wtp);
			if(ret == WCM_FAIL) {
				app_log (LOG_WARNING, "error: send_ac_bootstrap_status failed");
				return WCM_FAIL;
			}
			break;

		case MSG_TYPES__AP_STATUS:
			ret = send_resp_to_ac(!ret_val, sock, comm_msg_data->msg_type+1, comm_msg_data->request_id);
			if(ret == WCM_FAIL) {
				app_log (LOG_WARNING, "AC: Ack failed");
			}
			break;
		case MSG_TYPES__AC_CONFIG_REQ:
			if(ret_val == WCM_SUCCESS) {
				ret = send_ac_basic_config(sock, &ac_config);
				if(ret == WCM_FAIL) {
					app_log (LOG_WARNING, "send ac basic config failed");
				}
			}
			break;
		default:
			break;
	}
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int serve_cli(common_msg_t *comm_msg_data, int sock)
{
	int ret, ret_val = 0;
	switch(comm_msg_data->msg_type)
	{
		case MSG_TYPES__CLI_CREATE_VAP_REQ:
			{
				VapConfig *vap_msg;

				app_log (LOG_INFO, "CMD: CREATE_VAP");
				/*Unpack create vap data*/
				vap_msg = vap_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(vap_msg == NULL) {
					app_log (LOG_WARNING, "Error: Vap config unpack failed");
					return WCM_FAIL;
				}
				ret = process_create_vap_request(vap_msg);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				vap_config__free_unpacked(vap_msg, NULL);
			}
			break;
		case MSG_TYPES__CLI_CREATE_GROUP_REQ:
			{
				GroupConfig *group_msg;
				app_log (LOG_INFO, "CMD: CREATE_GROUP");

				/*Unpack create group data*/
				group_msg = group_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_msg == NULL) {
					app_log (LOG_WARNING, "Error: group config unpack failed");
					return WCM_FAIL;
				}
				ret = process_create_gcp_request(group_msg);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				group_config__free_unpacked(group_msg, NULL);
			}
			break;
		case MSG_TYPES__CLI_UPDATE_GROUP_REQ:
			{
				GroupConfig *group_msg;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: UPDATE_GROUP");

				/*Unpack create group data*/
				group_msg = group_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_msg == NULL) {
					app_log (LOG_WARNING, "Error: group config unpack failed");
					return WCM_FAIL;
				}
				ret = process_update_gcp_request(group_msg);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS) {
					memset(&update, 0, sizeof(gcp_vap_info_t));
					memcpy(update.groupID, group_msg->groupid, GROUP_ID_LEN);	
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_GROUP_UPDATE_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				group_config__free_unpacked(group_msg, NULL);
			}
			break;

		case MSG_TYPES__CLI_UPDATE_VAP_REQ:
			{
				VapConfig *vap_msg;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: UPDATE_VAP");
				/*Unpack create vap data*/
				vap_msg = vap_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(vap_msg == NULL) {
					app_log (LOG_WARNING, "Error: Vap config unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				ret = process_update_vap_request(vap_msg);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS){
					memset(&update, 0, sizeof(gcp_vap_info_t));
					memcpy(update.vapID, vap_msg->vapid, VAP_ID_LEN);	
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_VAP_UPDATE_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				/*free proto*/
				vap_config__free_unpacked(vap_msg, NULL);
			}
			break;
		case MSG_TYPES__CLI_GROUP_VAP_MAP_REQ:
			{
				GroupSsidConfig *group_ssid;
				app_log (LOG_INFO, "CMD: GROUP_SSID map");
				/*Unpack create group data*/
				group_ssid = group_ssid_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_ssid == NULL) {
					app_log (LOG_WARNING, "Error: group ssid table unpack failed");
					return WCM_FAIL;
				}
				ret = process_gcp_vap_map_request(group_ssid);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				group_ssid_config__free_unpacked(group_ssid, NULL);
			}
			break;
		case MSG_TYPES__CLI_GROUP_VAP_DELETE_REQ:
			{
				GroupSsidConfig *group_ssid;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: GROUP_SSID unmap");
				/*Unpack create group data*/
				group_ssid = group_ssid_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_ssid == NULL) {
					app_log (LOG_WARNING, "Error: group ssid table unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				memset(&update, 0, sizeof(gcp_vap_info_t));
				memcpy(update.vapID, group_ssid->vapid, VAP_ID_LEN);
				memcpy(update.groupID, group_ssid->groupid, GROUP_ID_LEN);	
				ret = process_gcp_vap_delete_request(&update);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS) {
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_GROUP_VAP_DELETE_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				/*free proto*/
				group_ssid_config__free_unpacked(group_ssid, NULL);
			}
			break;

		case MSG_TYPES__CLI_UNREG_AP_REQ:
			{
				UnregisterAP *command_unreg_ap = NULL;

				/*Unpack unreg data*/
				command_unreg_ap = unregister_ap__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(command_unreg_ap == NULL) {
					app_log (LOG_WARNING, "Error: Unpack unregister ap failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				ret = process_ap_unreg_request(command_unreg_ap);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				unregister_ap__free_unpacked(command_unreg_ap, NULL);
			}
			break;
		case MSG_TYPES__CLI_REG_AP_REQ:
			{
				RegisterAP *command_reg_ap = NULL;

				app_log (LOG_INFO, "CMD: REG_AP");
				/*Unpack reg data*/
				command_reg_ap = register_ap__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(command_reg_ap == NULL) {
					app_log (LOG_WARNING, "Error: Unpack reg AP");
					return WCM_FAIL;
				}
				ret = process_ap_reg_request(command_reg_ap);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				register_ap__free_unpacked(command_reg_ap, NULL);
			}
			break;
		case MSG_TYPES__CLI_DELETE_VAP_REQ:
			{
				VapConfig *vap_msg;
				gcp_vap_info_t update;
				wlc_log (LOG_INFO, "CMD: DELETE_VAP");
				/*Unpack create vap data*/
				vap_msg = vap_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(vap_msg == NULL) {
					wlc_log (LOG_WARNING, "Error: Vap config unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				ret = process_vap_delete_request(vap_msg->vapid);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS) {
					memset(&update, 0, sizeof(gcp_vap_info_t));
					memcpy(update.vapID, vap_msg->vapid, VAP_ID_LEN);	
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_VAP_DELETION_REQ);
					if(ret != WCM_SUCCESS) {
						wlc_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				/*free proto*/
				vap_config__free_unpacked(vap_msg, NULL);
			}
			break;
		case MSG_TYPES__CLI_DELETE_GROUP_REQ:
			{
				GroupConfig *group_msg;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: DELETE_GROUP");

				/*Unpack create group data*/
				group_msg = group_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_msg == NULL) {
					app_log (LOG_WARNING, "Error: group config unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				if(group_msg->groupid != NULL) {
					app_log (LOG_INFO, "groupId: %s", group_msg->groupid);
				}
				ret = process_group_delete_request(group_msg->groupid);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS) {
					memset(&update, 0, sizeof(gcp_vap_info_t));
					memcpy(update.groupID, group_msg->groupid, GROUP_ID_LEN);	
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_GROUP_DELETE_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				/*free proto*/
				group_config__free_unpacked(group_msg, NULL);
			}
			break;
		default:
			app_log (LOG_WARNING, "Message type %d not supported", comm_msg_data->msg_type);
			break;
	}
END:
	switch(comm_msg_data->msg_type) {
		case MSG_TYPES__CLI_CREATE_VAP_REQ:
		case MSG_TYPES__CLI_CREATE_GROUP_REQ:
		case MSG_TYPES__CLI_UPDATE_VAP_REQ:
		case MSG_TYPES__CLI_UPDATE_GROUP_REQ:
		case MSG_TYPES__CLI_GROUP_VAP_MAP_REQ:
		case MSG_TYPES__CLI_GROUP_VAP_DELETE_REQ:
		case MSG_TYPES__CLI_UNREG_AP_REQ:
		case MSG_TYPES__CLI_REG_AP_REQ:
		case MSG_TYPES__CLI_DELETE_GROUP_REQ:
		case MSG_TYPES__CLI_DELETE_VAP_REQ:
			ret = send_resp_to_cli(!ret_val, sock, comm_msg_data->msg_type+1, comm_msg_data->request_id);
			if(ret == WCM_FAIL) {
				app_log (LOG_WARNING, "CLI: Ack failed");
			}
			break;
		default:
			break;
	}

	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int serve_django(common_msg_t *comm_msg_data, int sock)
{
	int ret, ret_val=0;
	switch(comm_msg_data->msg_type)
	{
		case MSG_TYPES__DJANGO_REG_AP_REQ:
			{
				RegisterAP *command_reg_ap = NULL;

				app_log (LOG_INFO, "CMD: REG_AP");
				/*Unpack reg data*/
				command_reg_ap = register_ap__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(command_reg_ap == NULL) {
					app_log (LOG_WARNING, "Error: Unpack reg AP");
					return WCM_FAIL;
				}
				ret = process_ap_reg_request(command_reg_ap);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				register_ap__free_unpacked(command_reg_ap, NULL);
			}
			break;
		case MSG_TYPES__DJANGO_UNREG_AP_REQ:
			{
				/*TODO: processing and response of request to be updated */	
				UnregisterAP *command_unreg_ap = NULL;
				app_log (LOG_INFO, "CMD: UNREG_AP");

				/*Unpack unreg data*/
				command_unreg_ap = unregister_ap__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(command_unreg_ap == NULL) {
					app_log (LOG_WARNING, "Error: Unpack unregister ap failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				ret = process_ap_unreg_request(command_unreg_ap);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				unregister_ap__free_unpacked(command_unreg_ap, NULL);
			}
			break;
		case MSG_TYPES__DJANGO_WLC_CONFIG_REQ:
			{
				/*TODO: processing and response of request to be updated */	
			}
			break;
		case MSG_TYPES__DJANGO_IMG_UPGRADE_REQ:
			{
				ImageUpgrade *command_img_upgrade = NULL;
				app_log (LOG_INFO, "CMD: IMAGE UPGRADE");
				/*Unpack image upgrade data*/
				command_img_upgrade = image_upgrade__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(command_img_upgrade == NULL) {
					app_log (LOG_WARNING, "Error: Unpack image upgrade message failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				ret = process_img_upgrade_request(command_img_upgrade);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				image_upgrade__free_unpacked(command_img_upgrade, NULL);
			}
			break;
		case MSG_TYPES__DJANGO_CREATE_VAP_REQ:
			{
				VapConfig *vap_msg;

				app_log (LOG_INFO, "CMD: CREATE_VAP");
				/*Unpack create vap data*/
				vap_msg = vap_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(vap_msg == NULL) {
					app_log (LOG_WARNING, "Error: Vap config unpack failed");
					return WCM_FAIL;
				}
				ret = process_create_vap_request(vap_msg);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				vap_config__free_unpacked(vap_msg, NULL);
			}
			break;
		case MSG_TYPES__DJANGO_CREATE_GROUP_REQ:
			{
				GroupConfig *group_msg;
				app_log (LOG_INFO, "CMD: CREATE_GROUP");

				/*Unpack create group data*/
				group_msg = group_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_msg == NULL) {
					app_log (LOG_WARNING, "Error: group config unpack failed");
					return WCM_FAIL;
				}
				ret = process_create_gcp_request(group_msg);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				group_config__free_unpacked(group_msg, NULL);
			}
			break;
		case MSG_TYPES__DJANGO_UPDATE_GROUP_REQ:
			{
				GroupConfig *group_msg;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: UPDATE_GROUP");

				/*Unpack create group data*/
				group_msg = group_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_msg == NULL) {
					app_log (LOG_WARNING, "Error: group config unpack failed");
					return WCM_FAIL;
				}
				ret = process_update_gcp_request(group_msg);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS) {
					memset(&update, 0, sizeof(gcp_vap_info_t));
					memcpy(update.groupID, group_msg->groupid, GROUP_ID_LEN);	
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_GROUP_UPDATE_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				group_config__free_unpacked(group_msg, NULL);
			}
			break;

		case MSG_TYPES__DJANGO_UPDATE_VAP_REQ:
			{
				VapConfig *vap_msg;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: UPDATE_VAP");
				/*Unpack create vap data*/
				vap_msg = vap_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(vap_msg == NULL) {
					app_log (LOG_WARNING, "Error: Vap config unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				ret = process_update_vap_request(vap_msg);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS){
					memset(&update, 0, sizeof(gcp_vap_info_t));
					memcpy(update.vapID, vap_msg->vapid, VAP_ID_LEN);	
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_VAP_UPDATE_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				/*free proto*/
				vap_config__free_unpacked(vap_msg, NULL);
			}
			break;
		case MSG_TYPES__DJANGO_GROUP_VAP_MAP_REQ:
			{
				GroupSsidConfig *group_ssid;
				app_log (LOG_INFO, "CMD: GROUP_SSID map");
				/*Unpack create group data*/
				group_ssid = group_ssid_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_ssid == NULL) {
					app_log (LOG_WARNING, "Error: group ssid table unpack failed");
					return WCM_FAIL;
				}
				ret = process_gcp_vap_map_request(group_ssid);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				group_ssid_config__free_unpacked(group_ssid, NULL);
			}
			break;
		case MSG_TYPES__DJANGO_DELETE_VAP_REQ:
			{
				VapConfig *vap_msg;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: DELETE_VAP");
				/*Unpack create vap data*/
				vap_msg = vap_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(vap_msg == NULL) {
					app_log (LOG_WARNING, "Error: Vap config unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				ret = process_vap_delete_request(vap_msg->vapid);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS) {
					memset(&update, 0, sizeof(gcp_vap_info_t));
					memcpy(update.vapID, vap_msg->vapid, VAP_ID_LEN);	
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_VAP_DELETION_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				/*free proto*/
				vap_config__free_unpacked(vap_msg, NULL);
			}
			break;
		case MSG_TYPES__DJANGO_DELETE_GROUP_REQ:
			{
				GroupConfig *group_msg;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: DELETE_GROUP");

				/*Unpack create group data*/
				group_msg = group_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_msg == NULL) {
					app_log (LOG_WARNING, "Error: group config unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				if(group_msg->groupid != NULL) {
					app_log (LOG_INFO, "groupId: %s", group_msg->groupid);
				}
				ret = process_group_delete_request(group_msg->groupid);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS) {
					memset(&update, 0, sizeof(gcp_vap_info_t));
					memcpy(update.groupID, group_msg->groupid, GROUP_ID_LEN);	
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_GROUP_DELETE_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				/*free proto*/
				group_config__free_unpacked(group_msg, NULL);
			}
			break;
		case MSG_TYPES__DJANGO_GROUP_VAP_DELETE_REQ:
			{
				GroupSsidConfig *group_ssid;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: GROUP_SSID unmap");
				/*Unpack create group data*/
				group_ssid = group_ssid_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_ssid == NULL) {
					app_log (LOG_WARNING, "Error: group ssid table unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				memset(&update, 0, sizeof(gcp_vap_info_t));
				memcpy(update.vapID, group_ssid->vapid, VAP_ID_LEN);
				memcpy(update.groupID, group_ssid->groupid, GROUP_ID_LEN);	
				ret = process_gcp_vap_delete_request(&update);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS) {
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_GROUP_VAP_DELETE_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				/*free proto*/
				group_ssid_config__free_unpacked(group_ssid, NULL);
			}
			break;

		default:
			app_log (LOG_WARNING, "Message type %d not supported", comm_msg_data->msg_type);
			break;
	}
END:
	switch(comm_msg_data->msg_type) {
		case MSG_TYPES__DJANGO_REG_AP_REQ:
		case MSG_TYPES__DJANGO_UNREG_AP_REQ:
		case MSG_TYPES__DJANGO_IMG_UPGRADE_REQ:
		case MSG_TYPES__DJANGO_CREATE_VAP_REQ:
		case MSG_TYPES__DJANGO_CREATE_GROUP_REQ:
		case MSG_TYPES__DJANGO_UPDATE_VAP_REQ:
		case MSG_TYPES__DJANGO_UPDATE_GROUP_REQ:
		case MSG_TYPES__DJANGO_GROUP_VAP_MAP_REQ:
		case MSG_TYPES__DJANGO_DELETE_VAP_REQ:
		case MSG_TYPES__DJANGO_DELETE_GROUP_REQ:
		case MSG_TYPES__DJANGO_GROUP_VAP_DELETE_REQ:
			ret = send_resp_to_django(!ret_val, sock, comm_msg_data->msg_type+1, comm_msg_data->request_id);
			if(ret == WCM_FAIL) {
				app_log (LOG_WARNING, "Django: Ack failed");
			}
			break;
		default:
			break;
	}
	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int serve_snmp(common_msg_t *comm_msg_data, int sock)
{
	int ret, ret_val = 0;
	switch(comm_msg_data->msg_type)
	{
		case MSG_TYPES__SNMP_CREATE_VAP_REQ:
			{
				VapConfig *vap_msg;
				gcp_vap_info_t update;

				app_log (LOG_INFO, "CMD: CREATE_VAP");/*Same command for create vap or update vap*/
				/*Unpack create vap data*/
				vap_msg = vap_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(vap_msg == NULL) {
					app_log (LOG_WARNING, "Error: Vap config unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				/*VAP ID is must for SNMP unlike CLI*/
				if(vap_msg->vapid != NULL) {
					ret = cass_check_vap_presence(vap_msg->vapid);
				} else {
					app_log (LOG_WARNING, "Error: vap Id is null");
					/*free proto*/
					vap_config__free_unpacked(vap_msg, NULL);
					ret_val = WCM_FAIL;
					goto END;
				}
				if(ret == WCM_SUCCESS) {
					ret = process_update_vap_request(vap_msg);
					if(ret == WCM_FAIL) {
						ret_val = WCM_FAIL;
					} else if(ret == WCM_SUCCESS){
						memset(&update, 0, sizeof(gcp_vap_info_t));
						memcpy(update.vapID, vap_msg->vapid, VAP_ID_LEN);	
						ret = broadcast_to_ac(&update, MSG_TYPES__AP_VAP_UPDATE_REQ);
						if(ret != WCM_SUCCESS) {
							app_log (LOG_WARNING, "broadcast update to AC failed");
						}
					}
				} else {
					ret = process_create_vap_request(vap_msg);
				}
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				vap_config__free_unpacked(vap_msg, NULL);
			}
			break;
		case MSG_TYPES__SNMP_CREATE_GROUP_REQ:
			{
				GroupConfig *group_msg;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: CREATE_GROUP");/*Same command for create and update*/

				/*Unpack create group data*/
				group_msg = group_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_msg == NULL) {
					app_log (LOG_WARNING, "Error: group config unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}

				/*Group ID is must for SNMP unlike CLI*/
				if(group_msg->groupid != NULL) {
					ret = cass_check_gcp_presence(group_msg->groupid);
				} else {
					app_log (LOG_WARNING, "Error: group Id is null");
					/*free proto*/
					group_config__free_unpacked(group_msg, NULL);
					ret_val = WCM_FAIL;
					goto END;
				}
				if (ret == WCM_SUCCESS) {
					ret = process_update_gcp_request(group_msg);
					if(ret == WCM_FAIL) {
						ret_val = WCM_FAIL;
					} else if(ret == WCM_SUCCESS) {
						memset(&update, 0, sizeof(gcp_vap_info_t));
						memcpy(update.groupID, group_msg->groupid, GROUP_ID_LEN);	
						ret = broadcast_to_ac(&update, MSG_TYPES__AP_GROUP_UPDATE_REQ);
						if(ret != WCM_SUCCESS) {
							app_log (LOG_WARNING, "broadcast update to AC failed");
						}
					}
				} else {
					ret = process_create_gcp_request(group_msg);
				}
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				group_config__free_unpacked(group_msg, NULL);
			}
			break;
		case MSG_TYPES__SNMP_WLC_CONFIG_REQ:
			{
				/*TODO: processing and response of request to be updated */	
				WlcSystem *wlc_config = NULL;

				app_log (LOG_INFO, "CMD: WLC_CONFIG");
				wlc_config = wlc_system__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(wlc_config == NULL) {
					app_log (LOG_WARNING, "Error: Unpack WLC CONFIG failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				ret = process_set_wlc_config_request(wlc_config);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				wlc_system__free_unpacked(wlc_config, NULL);
			}
			break;
		case MSG_TYPES__SNMP_GROUP_VAP_MAP_REQ:
			{
				GroupSsidConfig *group_ssid;
				app_log (LOG_INFO, "CMD: GROUP_SSID map");
				/*Unpack create group data*/
				group_ssid = group_ssid_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_ssid == NULL) {
					app_log (LOG_WARNING, "Error: group ssid table unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				ret = process_gcp_vap_map_request(group_ssid);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				}
				/*free proto*/
				group_ssid_config__free_unpacked(group_ssid, NULL);
			}
			break;
		case MSG_TYPES__SNMP_DELETE_VAP_REQ:
			{
				VapConfig *vap_msg;
				gcp_vap_info_t update;

				app_log (LOG_INFO, "CMD: DELETE_VAP");
				/*Unpack create vap data*/
				vap_msg = vap_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(vap_msg == NULL) {
					app_log (LOG_WARNING, "Error: Vap config unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				ret = process_vap_delete_request(vap_msg->vapid);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS) {
					memset(&update, 0, sizeof(gcp_vap_info_t));
					memcpy(update.vapID, vap_msg->vapid, VAP_ID_LEN);	
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_VAP_DELETION_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				/*free proto*/
				vap_config__free_unpacked(vap_msg, NULL);
			}
			break;
		case MSG_TYPES__SNMP_DELETE_GROUP_REQ:
			{
				GroupConfig *group_msg;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: DELETE_GROUP");

				/*Unpack create group data*/
				group_msg = group_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_msg == NULL) {
					app_log (LOG_WARNING, "Error: group config unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				ret = process_group_delete_request(group_msg->groupid);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS) {
					memset(&update, 0, sizeof(gcp_vap_info_t));
					memcpy(update.groupID, group_msg->groupid, GROUP_ID_LEN);	
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_GROUP_DELETE_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				/*free proto*/
				group_config__free_unpacked(group_msg, NULL);
			}
			break;
		case MSG_TYPES__SNMP_GROUP_VAP_DELETE_REQ:
			{
				GroupSsidConfig *group_ssid;
				gcp_vap_info_t update;
				app_log (LOG_INFO, "CMD: GROUP_SSID unmap");
				/*Unpack create group data*/
				group_ssid = group_ssid_config__unpack(NULL, comm_msg_data->data_len, comm_msg_data->data);
				if(group_ssid == NULL) {
					app_log (LOG_WARNING, "Error: group ssid table unpack failed");
					ret_val = WCM_FAIL;
					goto END;
				}
				memset(&update, 0, sizeof(gcp_vap_info_t));
				memcpy(update.vapID, group_ssid->vapid, VAP_ID_LEN);
				memcpy(update.groupID, group_ssid->groupid, GROUP_ID_LEN);	
				ret = process_gcp_vap_delete_request(&update);
				if(ret == WCM_FAIL) {
					ret_val = WCM_FAIL;
				} else if(ret == WCM_SUCCESS) {
					ret = broadcast_to_ac(&update, MSG_TYPES__AP_GROUP_VAP_DELETE_REQ);
					if(ret != WCM_SUCCESS) {
						app_log (LOG_WARNING, "broadcast update to AC failed");
					}
				}
				/*free proto*/
				group_ssid_config__free_unpacked(group_ssid, NULL);
			}
			break;

		default:
			app_log (LOG_WARNING, "Message type %d not supported", comm_msg_data->msg_type);
			break;
	}
END:
	switch(comm_msg_data->msg_type) {
		case MSG_TYPES__SNMP_CREATE_VAP_REQ:
		case MSG_TYPES__SNMP_CREATE_GROUP_REQ:
		case MSG_TYPES__SNMP_GROUP_VAP_MAP_REQ:
		case MSG_TYPES__SNMP_WLC_CONFIG_REQ:
		case MSG_TYPES__SNMP_DELETE_VAP_REQ:
		case MSG_TYPES__SNMP_DELETE_GROUP_REQ:
		case MSG_TYPES__SNMP_GROUP_VAP_DELETE_REQ:
			ret = send_resp_to_snmp(!ret_val, sock, comm_msg_data->msg_type+1, comm_msg_data->request_id);
			if(ret == WCM_FAIL) {
				app_log (LOG_WARNING, "SNMP: Ack failed");
			}
			break;
		default:
			break;
	}

	if(ret_val != 0) {
		app_log (LOG_NOTICE, "%s: return %d", __func__, ret_val);
	}
	return ret_val;
}

int get_max_desc_dmi()
{
	int Max, pos;

	Max = dmi_sockets[0];
	for (pos = 1; pos < DMI_SOCKETS_COUNT; pos++)
	{
		if (dmi_sockets[pos] > Max)
		{
			Max = dmi_sockets[pos];
		}
	}
	return Max;
}

int get_max_desc_ac()
{
	int Max, pos;

	Max = cvm_arr[0].ac_socket;
	for (pos = 1; pos < AC_SOCKETS_COUNT; pos++)
	{
		if (cvm_arr[pos].ac_socket > Max)
		{
			Max = cvm_arr[pos].ac_socket;
		}
	}
	return Max;
}

int create_server(char *ip, unsigned short port)
{
	int sock;
	struct sockaddr_in server;
	int opt = 1;

	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		app_log (LOG_DEBUG, "%s socket %s", __func__, strerror(errno));
		return -1;
	}
    if (setsockopt (sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                &opt, sizeof(opt))) {
		app_log (LOG_DEBUG, "setsockopt sock: %s", strerror(errno));
        return -1;
    }
	server.sin_addr.s_addr = inet_addr(ip);
	server.sin_family = AF_INET;
	server.sin_port = htons(port);
	if(bind(sock,(struct sockaddr *)&server , sizeof(server)) < 0) {
		//print the error message
		app_log (LOG_ERR, "Bind failed on %s:%d Cause: %s", ip, port, strerror (errno));
		return WCM_FAIL;
	}
	app_log(LOG_INFO, "bind done");
	return sock;
}

int handle_ac_comm(unsigned char *buf, unsigned int *len, int *sock)
{
	fd_set readset;
	struct timeval t;
	int socket_index, ret, c;
	int ac_client_sock;
	struct sockaddr_in ac_client;

	c = sizeof(struct sockaddr_in);
	t.tv_usec=0;
	t.tv_sec= SOCKET_TIMEOUT_SECONDS;/*Check FD_ISSET for Timeout seconds*/
	FD_ZERO(&readset);/*Clear readset*/
	FD_SET(ac_server_sock, &readset);/*Set ac server socket descriptor if any incoming clients*/
	if(max_sd_ac != ac_server_sock)
	{
		for(socket_index = 0; socket_index < AC_SOCKETS_COUNT; socket_index++) {
			if(cvm_arr[socket_index].ac_socket > -1)
			FD_SET(cvm_arr[socket_index].ac_socket, &readset);/*Set all socket descriptor if anything to read*/
		}
	}

	ret = select(max_sd_ac+1, &readset, NULL, NULL, &t);/*FD_SETSIZE or (higher descriptor +1)*/
	if(ret > 0) { /*something to read*/
		if(FD_ISSET(ac_server_sock, &readset)) {
			/*Incoming client*/
			ac_client_sock = accept(ac_server_sock, (struct sockaddr *)&ac_client, (socklen_t*)&c);
			if(ac_client_sock <= 0) {
				app_log (LOG_WARNING, "accept ac_server_sock:%s", strerror(errno));
			} else {
				app_log (LOG_ALERT, "New ac client connected: sd %d", ac_client_sock);
				memset(cvm_ip, 0, sizeof(cvm_ip));
				ret = send(ac_client_sock, "GET_IP", strlen("GET_IP"), 0);
				if(ret > 0) {
					ret = recv(ac_client_sock, cvm_ip, sizeof(cvm_ip), 0);
					if(ret > 0) {
						app_log (LOG_ALERT, "cvm_ip[0]:%d, cvm_ip[1]: %d, cvn_ip[2]:%d, cvm_ip[3]:%d", cvm_ip[0], cvm_ip[1], cvm_ip[2], cvm_ip[3]);
					}
				}
				for(socket_index=0; socket_index < AC_SOCKETS_COUNT; socket_index++) {/*check free sockets and fill the current socket*/
					if(cvm_arr[socket_index].ac_socket == -1)
					{
						cvm_arr[socket_index].ac_socket = ac_client_sock;/*copy socket descriptor to array*/
						memset(cvm_arr[socket_index].ip, 0, sizeof(cvm_arr[socket_index].ip));
						memcpy(cvm_arr[socket_index].ip, cvm_ip, sizeof(cvm_arr[socket_index].ip));/*copy cvm ip to array*/
						ret = send_pending_unreg_msgs_to_cvm(cvm_ip, cvm_arr[socket_index].ac_socket);
						if(ret == WCM_FAIL) {
							app_log (LOG_WARNING, "Error: Sending Pending Unreg msgs to AC");
						}
						break;
					}
				}
				if(socket_index == AC_SOCKETS_COUNT)
				{
					app_log (LOG_EMERG, "ac socket array is full");
					close(ac_client_sock);
				}
			}
			max_sd_ac = get_max_desc_ac();/*update Max file descriptor number*/
			app_log (LOG_DEBUG, "new max_sd_ac %d", max_sd_ac);
		}
		/*Check incoming messages from clients*/
		for(socket_index = 0; socket_index < AC_SOCKETS_COUNT; socket_index++) {
			/*Check sockets after std read/write/error*/
			if(cvm_arr[socket_index].ac_socket > -1) {
				if(FD_ISSET(cvm_arr[socket_index].ac_socket, &readset)) {
					memset(buf, 0, MAX_RECV_BUFFER_SIZE);

					ret = sock_recv(cvm_arr[socket_index].ac_socket, buf, MAX_RECV_BUFFER_SIZE);
					if(ret > 0) {
						*len = ret;
						*sock = cvm_arr[socket_index].ac_socket;
						return DATA_RECV;
					} else if(ret <= 0) {
						if(ret < 0) {
							app_log (LOG_WARNING, "recv cvm_arr[%d].ac_socket: %s", socket_index,  strerror(errno));
						}
						if(ret == 0 ) {/*An orderly shutdown*/
							app_log (LOG_ALERT, "Ac client disconnected, AC IP: %d.%d.%d.%d", \
							cvm_arr[socket_index].ip[0], cvm_arr[socket_index].ip[1], \
							cvm_arr[socket_index].ip[2], cvm_arr[socket_index].ip[3]);
						}
						close(cvm_arr[socket_index].ac_socket);
						cvm_arr[socket_index].ac_socket = -1;
						memset(cvm_arr[socket_index].ip, 0, sizeof(cvm_arr[socket_index].ip));
					}
				}
			}
		}
	} else if(ret < 0) {
		app_log (LOG_WARNING, "%s select: %s", __func__, strerror(errno));
	} else if(ret == 0) {
		/*app_log (LOG_ERR, "select timeout");*/
	}
	return 0;
}

void * ac_handler(void * arg)
{
	int ret, sock;
	unsigned int recv_len, left_len;
	unsigned char buf[MAX_RECV_BUFFER_SIZE];
	unsigned char *ptr;
	app_log(LOG_INFO, "AC thread created");
	/*Create AC server scoket*/
	ac_server_sock = create_server(mvm_ip, WCM_AC_PORT);
	if(ac_server_sock <= 0) {
		app_log (LOG_ERR, "Error: ac server create failed");
		goto CLEAN;
	}

	ret = listen(ac_server_sock, AC_LISTEN_COUNT);
	if(ret < 0) {
		app_log (LOG_WARNING, "listen ac_server_sock: %s", strerror(errno));
		close(ac_server_sock);
		goto CLEAN;
	}
	max_sd_ac = ac_server_sock;
	while(1) {
		/*Process AC clients communication*/
		memset(&buf, 0, sizeof(buf));
		if(handle_ac_comm(buf, &recv_len, &sock) == DATA_RECV) {
			common_msg_t *comm_msg_proto_recv=NULL;
			common_msg_t test_msg;
			app_log (LOG_DEBUG, "AC: data recv len :%d", recv_len);
			ptr = buf;
			left_len = recv_len;
NEXT_PKT:
			memset(&test_msg, 0, sizeof(common_msg_t));
			memcpy(&test_msg, ptr, sizeof(common_msg_t));
			if(left_len >= sizeof(common_msg_t)+test_msg.data_len) {/*TODO: check packet_len validity*/
				app_log(LOG_DEBUG, "interface : %u, msg_type: %u, response:%u, data_len:%u: request id:%u", test_msg.interface, test_msg.msg_type, test_msg.response, test_msg.data_len, test_msg.request_id);
				if(test_msg.sof != MSG_SOF) {
					app_log(LOG_NOTICE, "SOF is not found");
					continue;
				}
				comm_msg_proto_recv = malloc(sizeof(common_msg_t) + test_msg.data_len);
				if(comm_msg_proto_recv == NULL) {
					app_log (LOG_ERR, "malloc common_msg_t *comm_msg_proto_recv: %s", strerror(errno));
					continue;
				}
				memset(comm_msg_proto_recv, 0, sizeof(common_msg_t) + test_msg.data_len);
				memcpy(comm_msg_proto_recv, ptr, sizeof(common_msg_t));
				memcpy(comm_msg_proto_recv->data, ptr+sizeof(common_msg_t), test_msg.data_len);
				switch(comm_msg_proto_recv->interface)
				{
					case IF_TYPE__WCM_AC_INTERFACE:
						ret = serve_ac(comm_msg_proto_recv, sock);
						if(ret < 0) {
							app_log (LOG_WARNING, "AC request process failed");
						}
						break;
					default:
						app_log (LOG_WARNING, "Interface type %d not supported", comm_msg_proto_recv->interface);
						break;
				}

				free(comm_msg_proto_recv);
				comm_msg_proto_recv = NULL;
				ptr += (sizeof(common_msg_t) + test_msg.data_len);
				left_len -= (sizeof(common_msg_t) + test_msg.data_len);
				if(left_len > 0) {
					app_log(LOG_DEBUG, "left_len: %d, msg_len: %ld", left_len, sizeof(common_msg_t) + test_msg.data_len);
					goto NEXT_PKT;
				} else {
					continue;
				}
			} else {
				/*process incomplete packet*/
				memcpy(buf, ptr, left_len);/*copy last data*/
				ptr = buf;
				memset(ptr+left_len, 0, MAX_RECV_BUFFER_SIZE-left_len);

				if(left_len < sizeof(common_msg_t) && ((select_call_read_check(sock)) == WCM_SUCCESS)) {/*get structure first*/
					ret = recv(sock, ptr+left_len, sizeof(common_msg_t) - left_len, 0);
					if(ret == (sizeof(common_msg_t) - left_len) && ((select_call_read_check(sock)) == WCM_SUCCESS)) {
						memset(&test_msg, 0, sizeof(test_msg));
						memcpy(&test_msg, ptr, left_len+ret);
						/*structure is complete, get pending data*/
						ret = recv(sock, ptr+left_len+ret, test_msg.data_len, 0);
						if(ret == test_msg.data_len) {
							left_len = sizeof(common_msg_t)+test_msg.data_len;/*last packet len*/
							goto NEXT_PKT;
						} else {
							app_log (LOG_INFO, "AC pending data read failed");
						}
					} else {
						app_log (LOG_INFO, "AC pending structure read failed");
					}
				} else if(left_len >= sizeof(common_msg_t)&&((select_call_read_check(sock)) == WCM_SUCCESS)) {
					/*structure is complete, get pending data*/
					ret = recv(sock, ptr+left_len, sizeof(common_msg_t)+test_msg.data_len-left_len, 0);
					if(ret == sizeof(common_msg_t)+test_msg.data_len-left_len) {
						left_len = sizeof(common_msg_t)+test_msg.data_len;/*last packet len*/
						goto NEXT_PKT;
					} else {
						app_log (LOG_INFO, "dmi pending data read failed");
					}
				}
			}
		}
	}
CLEAN:
	app_log (LOG_CRIT, "Thread Exit %s", __func__);
	exit(0);
}

int handle_dmi_comm(unsigned char *buf, unsigned int *len, int *sock)
{
	fd_set readset;
	struct timeval t;
	int socket_index, ret, c;
	int dmi_client_sock;
	struct sockaddr_in dmi_client;

	c = sizeof(struct sockaddr_in);
	t.tv_usec=0;
	t.tv_sec= SOCKET_TIMEOUT_SECONDS;/*Check FD_ISSET for Timeout seconds*/
	FD_ZERO(&readset);/*Clear readset*/
	FD_SET(dmi_server_sock, &readset);/*Set dmi server socket descriptor if any incoming clients*/
	if(max_sd != dmi_server_sock)
	{
		for(socket_index = 0; socket_index < DMI_SOCKETS_COUNT; socket_index++) {
			if(dmi_sockets[socket_index] > -1)
			FD_SET(dmi_sockets[socket_index], &readset);/*Set all socket descriptor if anything to read*/
		}
	}

	ret = select(max_sd+1, &readset, NULL, NULL, &t);/*FD_SETSIZE or (higher descriptor +1)*/
	if(ret > 0) { /*something to read*/
		if(FD_ISSET(dmi_server_sock, &readset)) {
			/*Incoming client*/
			dmi_client_sock = accept(dmi_server_sock, (struct sockaddr *)&dmi_client, (socklen_t*)&c);
			if(dmi_client_sock <= 0) {
				app_log (LOG_WARNING, "accept dmi_server_sock: %s", strerror(errno));
			} else {
				app_log (LOG_ALERT, "New dmi client connected: sd %d", dmi_client_sock);

				for(socket_index=0; socket_index < DMI_SOCKETS_COUNT; socket_index++) {/*check free sockets and fill the current socket*/
					if(dmi_sockets[socket_index] == -1)
					{
						dmi_sockets[socket_index] = dmi_client_sock;/*copy socket descriptor to array*/
						/*TODO need to update dmi_sockets structure with dmi interface name*/
						break;
					}
				}
				if(socket_index == DMI_SOCKETS_COUNT)
				{
					app_log (LOG_EMERG, "dmi socket array is full");
					close(dmi_client_sock);
				}
			}
			max_sd = get_max_desc_dmi();/*update Max file descriptor number*/
			app_log (LOG_DEBUG, "new max_sd %d", max_sd);
		}
		/*Check incoming messages from clients*/
		for(socket_index = 0; socket_index < DMI_SOCKETS_COUNT; socket_index++) {
			/*Check sockets after std read/write/error*/
			if(dmi_sockets[socket_index] > -1) {
				if(FD_ISSET(dmi_sockets[socket_index], &readset)) {
					memset(buf, 0, MAX_RECV_BUFFER_SIZE);

					ret = sock_recv(dmi_sockets[socket_index], buf, MAX_RECV_BUFFER_SIZE);
					if(ret > 0) {
						*len = ret;
						*sock = dmi_sockets[socket_index];
						return DATA_RECV;
					} else if(ret <= 0) {
						if(ret < 0) {
							app_log (LOG_WARNING, "recv dmi_sockets[%d]: %s", socket_index, strerror(errno));
						}
						if(ret == 0 ) {/*An orderly shutdown*/
							app_log (LOG_ALERT, "dmi client disconnected");
						}
						close(dmi_sockets[socket_index]);
						dmi_sockets[socket_index] = -1;
					}
				}
			}
		}
	} else if(ret < 0) {
		app_log (LOG_WARNING, "%s select: %s", __func__, strerror(errno));
	} else if(ret == 0) {
		/*app_log (LOG_ERR, "select timeout");*/
	}
	return 0;
}

char wlc_pname[APP_NAME_LEN];

int main (int argc, char **argv)
{
	int ret, sock, arr;
	unsigned int recv_len, left_len;
	pthread_t ac_tid, bootstrap_tid;/*Thread ID*/
	unsigned char buf[MAX_RECV_BUFFER_SIZE];
	unsigned char *ptr;

	wlc_setup_proc_info (wlc_pname, argv[0]);

	for(arr=0; arr<DMI_SOCKETS_COUNT; arr++) {
		dmi_sockets[arr] = -1;
	}
	for(arr=0; arr<AC_SOCKETS_COUNT; arr++) {
		cvm_arr[arr].ac_socket = -1;
		memset(cvm_arr[arr].ip, 0, IPV4_LEN);
	}
	/* Get the MVM IP from the shared file /tmp/shared/MVM_FILE */
	memset(mvm_ip, 0, sizeof(mvm_ip));
	if(get_interface_ip("MVM_FILE", mvm_ip) == FAILURE) {
		app_log (LOG_CRIT, "Getting MVM IP failed: %s", strerror(errno));
		return WCM_FAIL;
	}
	if (setup_wlcl_mutex(&wlcl_ctx_head) != 0) { /* Create Mutex lock to the List */
		app_log (LOG_CRIT, "create lock wlcl_ctx_head failed");
		return WCM_FAIL;    /* return when mutex lock not created */
	}

	if(pthread_create(&bootstrap_tid, NULL, bootstrap_thread, NULL) != 0) {
		app_log (LOG_CRIT, "Create Thread bootstrap_handler: %s", strerror(errno));
		return WCM_FAIL;
	}

	if(pthread_create(&ac_tid, NULL, ac_handler, NULL) != 0) {
		app_log (LOG_CRIT, "Create Thread ac_handler:%s", strerror(errno));
		return WCM_FAIL;
	}

	dmi_server_sock = create_server(mvm_ip, WCM_DMI_PORT);
	if(dmi_server_sock <= 0) {
		app_log (LOG_CRIT, "Error: dmi server create failed");
		goto CLEAN;
	}

	ret = listen(dmi_server_sock, DMI_LISTEN_COUNT);
	if(ret < 0) {
		app_log(LOG_CRIT, "listen dmi_server_sock: %s", strerror(errno));
		goto CLEAN;
	}
	max_sd = dmi_server_sock;
	while(1) {
		if(handle_dmi_comm(buf, &recv_len, &sock) == DATA_RECV)
		{
			common_msg_t *comm_msg_proto_recv = NULL;
			common_msg_t test_msg;/*read structure to receive data length*/
			app_log (LOG_DEBUG, "dmi: data recv len :%d", recv_len);
			ptr = buf;
			left_len = recv_len;
NEXT_PKT:
			memset(&test_msg, 0, sizeof(common_msg_t));
			memcpy(&test_msg, ptr, sizeof(common_msg_t));
			if(left_len >= sizeof(common_msg_t)+test_msg.data_len) {
				app_log(LOG_DEBUG, "interface : %u, msg_type: %u, response:%u, data_len:%u: request id:%u", test_msg.interface, test_msg.msg_type, test_msg.response, test_msg.data_len, test_msg.request_id);
				if(test_msg.sof != MSG_SOF) {
					app_log(LOG_NOTICE, "SOF is not found");
					continue;
				}
				comm_msg_proto_recv = malloc(sizeof(common_msg_t) + test_msg.data_len);
				if(comm_msg_proto_recv == NULL) {
					app_log (LOG_ERR, "malloc common_msg_t *comm_msg_proto_recv: %s", strerror(errno));
					continue;
				}
				memset(comm_msg_proto_recv, 0, sizeof(common_msg_t) + test_msg.data_len);
				memcpy(comm_msg_proto_recv, ptr, sizeof(common_msg_t));
				memcpy(comm_msg_proto_recv->data, ptr+sizeof(common_msg_t), test_msg.data_len);
				switch(comm_msg_proto_recv->interface)
				{
					case IF_TYPE__WCM_DJANGO_INTERFACE:
						ret = serve_django(comm_msg_proto_recv, sock);
						if(ret < 0) {
							app_log (LOG_WARNING, "django request process failed");
						}
						break;
					case IF_TYPE__WCM_SNMP_INTERFACE:
						ret = serve_snmp(comm_msg_proto_recv, sock);
						if(ret < 0) {
							app_log (LOG_WARNING, "snmp request process failed");
						}
						break;
					case IF_TYPE__WCM_CLI_INTERFACE:
						ret = serve_cli(comm_msg_proto_recv, sock);
						if(ret < 0) {
							app_log (LOG_WARNING, "CLI request process failed");
						}
						break;

					default:
						app_log (LOG_WARNING, "Interface type %d not supported", comm_msg_proto_recv->interface);
						break;
				}

				free(comm_msg_proto_recv);
				comm_msg_proto_recv = NULL;
				ptr += (sizeof(common_msg_t) + test_msg.data_len);
				left_len -= (sizeof(common_msg_t) + test_msg.data_len);
				if(left_len > 0) {
					app_log(LOG_DEBUG, "left_len: %d, msg_len: %ld", left_len, sizeof(common_msg_t) + test_msg.data_len);
					goto NEXT_PKT;
				} else {
					continue;
				}
			} else {
				/*process incomplete packet*/
				memcpy(buf, ptr, left_len);/*copy last data*/
				ptr = buf;
				memset(ptr+left_len, 0, MAX_RECV_BUFFER_SIZE-left_len);

				if((left_len < sizeof(common_msg_t))&&((select_call_read_check(sock)) == WCM_SUCCESS)) {/*get structure first*/
					ret = recv(sock, ptr+left_len, sizeof(common_msg_t) - left_len, 0);
					if(ret == (sizeof(common_msg_t) - left_len)&& ((select_call_read_check(sock)) == WCM_SUCCESS)) {
						memset(&test_msg, 0, sizeof(test_msg));
						memcpy(&test_msg, ptr, left_len+ret);
						/*structure is complete, get pending data*/
						app_log (LOG_DEBUG, "\nmsg_len: %d\n", test_msg.data_len);
						ret = recv(sock, ptr+left_len+ret, test_msg.data_len, 0);
						if(ret == test_msg.data_len) {
							left_len = sizeof(common_msg_t)+test_msg.data_len;/*last packet len*/
							goto NEXT_PKT;
						} else {
							app_log (LOG_INFO, "dmi pending data read failed");
						}
					} else {
						app_log (LOG_INFO, "dmi pending structure read failed");
					}
				} else if(left_len >= sizeof(common_msg_t)&& ((select_call_read_check(sock)) == WCM_SUCCESS)) {
					/*structure is complete, get pending data*/
					ret = recv(sock, ptr+left_len, sizeof(common_msg_t)+test_msg.data_len-left_len, 0);
					if(ret == sizeof(common_msg_t)+test_msg.data_len-left_len) {
						left_len = sizeof(common_msg_t)+test_msg.data_len;/*last packet len*/
						goto NEXT_PKT;
					} else {
						app_log (LOG_INFO, "dmi pending data read failed");
					}
				}
			}
		}
	}
CLEAN:
	return 0;
}
