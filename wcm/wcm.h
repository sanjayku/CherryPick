/********************************************************************************
 *
 *  @file        :  wcm.h
 *  @brief       :  Header for WLC Config Manager Server
 *  @author      :  VVDN Technologies Pvt. Ltd.
 *  Copyright    :  (c) 2017-2018 , VVDN Technologies Pvt. Ltd.
 Permission is hereby granted to everyone in VVDN Technologies
 to use the Software without restriction,including without
 limitation the rights to use, copy, modify, merge, publish,
 distribute, distribute with modifications.
 *
 ********************************************************************************/
#ifndef __WCM_HEADER__
#define __WCM_HEADER__
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include "wlc_common.pb-c.h"
#include "django_wcm.pb-c.h"
#include "snmp_wcm.pb-c.h"
#include "cli_wcm.pb-c.h"
#include "wlc_ports.h"
#include "bootstrap.h"
#include "wcm_ac_common.h"
#include "wlcl.h"
#include "wlc_gen_macro.h"
#include "wlc_ap_info_struct.h"
#include "wlc_gcp_struct.h"
#include "wlc_vap_struct.h"
#include "wlc_common_struct.h"
#include "wlc_system_struct.h"
#include "wlc_version.h"
#include "wlc_log.h"

#define SUCCESS 0   /* Returns if successful execution */
#define FAILURE -1  /* Returns if execution fail */
#define WCM_FAIL -1
#define WCM_SUCCESS 0
#define SOCKET_TIMEOUT_SECONDS 5
#define DMI_LISTEN_COUNT 10
#define DMI_SOCKETS_COUNT 10/*CLI, SNMP, DJANGO*/
#define AC_LISTEN_COUNT 10
#define AC_SOCKETS_COUNT 15 /*TODO: Limit AC clients*/
#define RECV_BUFFER_SIZE 1024
#define SEND_BUFFER_SIZE 1024
#define MAX_RECV_BUFFER_SIZE 4096
#define DATA_RECV 2
#define WCM_DEBUG_LOG 0
#define IP_STR_LENGTH 16
#define MASK_WLC_SYSNAME 0x01
#define MASK_WLC_LOCATION 0x02
#define MASK_WLC_TIMEZONE 0x04
#define MASK_WLC_IP 0x08

#define WLC_UPGRADE_CMD "/tmp/MVM_APPS/wlc_apps_upgrade.sh"

/**
 * @structure          : cvm_info_t
 * @brief              : structure to store cvm ip and socket descriptor
 * @members            : unsigned char ip[IPV4_LEN] - ip address in bytes
 *                       int ac_socket - AC socket descriptor
 */

typedef struct cvm_info {
	unsigned char ip[IPV4_LEN];
	int ac_socket;
} cvm_info_t;

/**
 * @structure          : bootstrap_t
 * @brief              : structure to store bootstrapped mac address in
 *                       linked list
 * @members            : head_ptr - structure of type list_head with next
 *                                  and prev links of type struct list_head
 *                       unsigned char mac[MAC_LEN] - mac addr array of len MAC_LEN
 */

typedef struct bootstrap {
    struct list_head head_ptr;
    unsigned char mac[MAC_LEN];
} bootstrap_t;

/**
 * @function           : select_call_read_check
 * @brief              : checks any data available to read
 * @input              : int sock - socket descriptor
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */
int select_call_read_check(int sock);

/**
 * @function           : sock_send
 * @brief              : send data to the socket descriptor
 * @input              : int sock - socket descriptor to send data to
 *                       unsigned char *buf - pointer to data to send
 *                       unsigned int len - length of data to send
 * @output             : None
 * @return             : number of bytes sent
 */

int sock_send(int sock, unsigned char *buf, unsigned int len);

/**
 * @function           : update_unreg_status_to_ac
 * @brief              : send unreg message to AC, if failed write same
                         in DB to make WCM retry when AC connects back.   
 * @input              : ap_metadata_t *ap_meta - data used to fetch AP information
 *                       wtp_basic_details_t *wtp_info - wtp details sent to AC
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int update_unreg_status_to_ac(ap_metadata_t *ap_meta, wtp_basic_details_t *wtp_info);


/**
 * @function           : send_pending_unreg_msgs_to_cvm
 * @brief              : send pending unreg messages to AC
 * @input              : unsigned char *cvm_ip - cvm ip to fetch unreg MACs
 *                       int sock - socket descriptor of AC
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int send_pending_unreg_msgs_to_cvm(unsigned char *cvm_ip, int sock);

/**
 * @function           : get_cvmip_from_sock
 * @brief              : fetch cvm ip for socket descriptor present in cvm_arr 
 * @input              : int sock - socket descriptor associated to CVM
 * @output             : unsigned char *ip - ip address of CVM
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int get_cvmip_from_sock(int sock, unsigned char *ip);


/**
 * @function           : get_sock_from_cvmip
 * @brief              : fetch socket descriptor for cvm ip present in cvm_arr 
 * @input              : unsigned char *ip - ip address of CVM
 * @output             : int *sock - socket descriptor associated to CVM
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int get_sock_from_cvmip(unsigned char *ip, int *sock);

/**
 * @function           : delete_mac_from_bsl
 * @brief              : searches for mac in global linked list and delete from list
 * @input              : unsigned char *mac - mac address to search in node
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int delete_mac_from_bsl(unsigned char *mac);

/**
 * @function           : check_bootstrap
 * @brief              : search for MAC if it is bootstrapped
 * @input              : unsigned char *mac - mac address to search in bootstrap list
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int check_bootstrap(unsigned char *macaddr);

/**
 * @function           : send_proto_common_msg
 * @brief              : pack common message and send to socket sock 
 * @input              : sock - socket descriptor to which common message is sent
 *                       common_msg_t *msg- commom message proto
 *                       int len- length to be sent
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int send_proto_common_msg(int sock, common_msg_t *msg, int len);

/**
 * @function           : process_ac_config_request
 * @brief              : fill ac_config message
 * @input              : ac_basic_config_t *ac_config - pointer to ac configuration
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_ac_config_request(ac_basic_config_t *ac_config);

/**
 * @function           : process_img_upgrade_request
 * @brief              : parse message for image file path and initiate image upgrade
 * @input              : ImageUpgrade *command_img_upgrade - pointer to image upgrade request
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_img_upgrade_request(ImageUpgrade *command_img_upgrade);

/**
 * @function           : process_set_ap_status_request
 * @brief              : parse ap_status_request and process it
 * @input              : wtp_basic_details_t *wtp - pointer to wtp info
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_set_ap_status_request(wtp_basic_details_t *wtp);

/**
 * @function           : process_check_bootstrap_request
 * @brief              : check AP is bootstrapped and send response to AC
 * @input              : wtp_basic_details_t - pointer to wtp info
 *                       sock - socket descriptor of AC
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_check_bootstrap_request(wtp_basic_details_t *wtp, int sock);

/**
 * @function           : process_vap_delete_request
 * @brief              : deletes vap from DB
 * @input              : char *vapid - vap ID
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_vap_delete_request(char * vapid);

/**
 * @function           : process_group_delete_request
 * @brief              : deletes group from DB
 * @input              : char *groupid - group ID
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_group_delete_request(char * groupid);

/**
 * @function           : process_gcp_vap_delete_request
 * @brief              : delete vap from a group
 * @input              : gcp_vap_info_t *group_ssid - group-vap details
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_gcp_vap_delete_request(gcp_vap_info_t *group_ssid);

/**
 * @function           : process_gcp_vap_map_request
 * @brief              : parse request and add vap to gcp-vap map table
 * @input              : GroupSsidConfig *group_ssid - group-vap proto message
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_gcp_vap_map_request(GroupSsidConfig *group_ssid);

/**
 * @function           : process_set_wlc_config_request
 * @brief              : parse request and set wlc configuration
 * @input              : WlcSystem *wlc_config
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_set_wlc_config_request(WlcSystem *wlc_config);

/**
 * @function           : process_update_gcp_request
 * @brief              : parse request and update gcp
 * @input              : GroupConfig *group_msg - group configuration proto message
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_update_gcp_request(GroupConfig *group_msg);

/**
 * @function           : process_create_gcp_request
 * @brief              : parse request and create new gcp
 * @input              : GroupConfig *group_msg - group configuration proto message
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_create_gcp_request(GroupConfig *group_msg);

/**
 * @function           : process_update_vap_request
 * @brief              : parse request and update vap
 * @input              : VapConfig *vap_msg - vap configuration proto message
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_update_vap_request(VapConfig *vap_msg);

/**
 * @function           : process_create_vap_request
 * @brief              : parse request and create new vap
 * @input              : VapConfig *vap_msg - vap configuration proto message
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_create_vap_request(VapConfig *vap_msg);

/**
 * @function           : process_ap_reg_request
 * @brief              : read reg message and register a AP
 * @input              : RegisterAP *command_reg_ap, holds register AP details
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_ap_reg_request(RegisterAP *command_reg_ap);

/**
 * @function           : process_ap_unreg_request
 * @brief              : read unreg message and unregister AP
 * @input              : UnregisterAP *command_unreg_ap, holds unregister AP details
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int process_ap_unreg_request(UnregisterAP *command_unreg_ap);

/**
 * @function           : send_ac_basic_config
 * @brief              : fetch ac_basic_config from DB and send to AC
 * @input              : int sock - socket descriptor of connected AC interface
 *                       struct ac_basic_config *config - strcuture holding ac basic configurations
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int send_ac_basic_config(int sock, struct ac_basic_config *config);

/**
 * @function           : send_unreg_wtp_to_ac
 * @brief              : send unregistered wtp info(wtp_id) to AC
 * @input              : wtp_basic_details_t *wtp_info structure to WTP info
 *                       int sock - socket descriptor of AC
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int send_unreg_wtp_to_ac(wtp_basic_details_t *wtp_info, int sock);


/**
 * @function           : send_resp_to_django
 * @brief              : send django response
 * @input              : int status - ACK(1),NACK(0)
 *                       int sock - socket descriptor of django interface
 *                       MsgTypes msg_type - status msg_type same as request message type
 *                       int random_id - random id for a message
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int send_resp_to_django(int status, int sock, MsgTypes msg_type, int random_id);

/**
 * @function           : send_resp_to_cli
 * @brief              : send CLI response
 * @input              : int status - ACK(1),NACK(0)
 *                       int sock - socket descriptor of CLI interface
 *                       MsgTypes msg_type - status msg_type same as request message type
 *                       int random_id - random id for a message
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int send_resp_to_cli(int status, int sock, MsgTypes msg_type, int random_id);


/**
 * @function           : send_resp_to_snmp
 * @brief              : send snmp response
 * @input              : int status - ACK(1),NACK(0)
 *                       int sock - socket descriptor of snmp interface
 *                       MsgTypesSnmp msg_type - status msg_type same as request message type
 *                       int random_id - random id for a message
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int send_resp_to_snmp(int status, int sock, MsgTypes msg_type, int random_id);

/**
 * @function           : send_resp_to_ac
 * @brief              : send AC response
 * @input              : int status - ACK(1),NACK(0)
 *                       int sock - socket descriptor of AC interface
 *                       int msg_type - status msg_type same as request message type
 *                       int random_id - random id for a message
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int send_resp_to_ac(int status, int sock, int msg_type, int random_id);


/**
 * @function           : send_ap_bootstrap_st_to_ac
 * @brief              : send bootstrap status
 * @input              : int status - ACK(1),NACK(0)
 *                       int sock - socket descriptor of AC client
 *                       struct wtp_basic_details *wtp
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int send_ap_bootstrap_st_to_ac(int status, int sock, struct wtp_basic_details *wtp);

/**
 * @function           : serve_ac
 * @brief              : serves the request from AC interface
 * @input              : common_msg_t *data - received data in protobuf
 *                       int sock - socket descriptor of connected interface
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int serve_ac(common_msg_t *data, int sock);

/**
 * @function           : handle_ac_comm
 * @brief              : handles ac communication
 * @input              : none
 * @output             : unsigned char *buf - recv data in output buffer
 *                       unsigned int *len - out length of received len
 *                       int *sock - out socket descriptor of connected AC
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int handle_ac_comm(unsigned char *buf, unsigned int *len, int *sock);

/**
 * @function           : serve_cli
 * @brief              : serves the request from CLI interface
 * @input              : common_msg_t *data - received data in structure
 *                       int sock - socket descriptor of connected interface
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int serve_cli(common_msg_t *data, int sock);

/**
 * @function           : serve_django
 * @brief              : serves the request from Django interface
 * @input              : common_msg_t *data- received data in structure
 *                       int sock - socket descriptor of connected interface
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int serve_django(common_msg_t *data, int sock);

/**
 * @function           : serve_snmp
 * @brief              : serves the request from SNMP interface
 * @input              : common_msg_t *data - received data in structure
 *                       int sock - socket descriptor of connected interface
 * @output             : None
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int serve_snmp(common_msg_t *data, int sock);

/**
 * @function           : sock_recv
 * @brief              : receive data from sock and store into buf
 * @input              : int sock - socket descriptor to recv data
 *                       unsigned int max_len - max length allowed to receive
 * @output             : unsigned char *buf - received data is stored in buf as output
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int sock_recv(int sock, unsigned char *buf, unsigned int max_len);

/**
 * @function           : get_max_desc_dmi
 * @brief              : find the max of socket descriptor from dmi_sockets array 
 * @input              : none
 * @output             : none
 * @return             : int type Max value of socket descriptor
 */

int get_max_desc_dmi();

/**
 * @function           : create_server
 * @brief              : creates socket and binds it to IP and Port
 * @input              : char *ip - ip address
 *                       unsigned short port - port number
 * @output             : none
 * @return             : status
 *                       socket - success
 *                       -1 - failure
 */

int create_server(char *ip, unsigned short port);

/**
 * @Callback           : ac_handler
 * @brief              : thread handler to process AC communication
 * @input              : void *arg - argument passed by pthread_create
 * @output             : none
 * @return             : status
 *                       NULL pointer - success
 *                       pointer with error - failure
 */

void * ac_handler(void * arg);

/**
 * @function           : handle_dmi_comm
 * @brief              : handles dmi communication
 * @input              : none
 * @output             : unsigned char *buf - recv data in output buffer
 *                       unsigned int *len - out length of received len
 *                       int *sock - out socket descriptor of connected AC
 * @return             : status
 *                       0 - success
 *                       -1 - failure
 */

int handle_dmi_comm(unsigned char *buf, unsigned int *len, int *sock);
#endif
