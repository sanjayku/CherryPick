#include "wcm.h"
#include "wcm_write.h"

int reply_to_client (int sock_desc, const void *ptr, size_t nbytes) {
    int nwritten, nrem;
    const void *tptr;

    nrem = nbytes;
    tptr = ptr;
    while(nrem > 0) {
        if((nwritten = send(sock_desc, tptr, nbytes, 0)) <= 0) {
            if(errno == EINTR)
                nwritten = 0;
            else
                return FAILURE;
        }
        nrem -= nwritten;
        tptr   += nwritten;
    }
    return nbytes; /* Return number of bytes written to the socket */
}
