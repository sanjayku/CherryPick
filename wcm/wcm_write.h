#ifndef __WCM_WRITE_H_INCLUDE__
#define __WCM_WRITE_H_INCLUDE__

/*******************************************************************************
 * @function    : reply_to_client
 * @brief       : This function will writes the structure to the client socket
 *                of size nbytes
 * @inputs      : int sock_desc - Socket Description to which data to be written
 *                const void *ptr - Message to be written
 *                size_t written - Number of bytes to be written
 * @output      : Computes the number of bytes written
 * @return_type : Number of bytes written to the socket
 ******************************************************************************/

int reply_to_client (int sockfd, const void *ptr, size_t nbytes);

#endif
